#!/usr/bin/python

import sys
from PySide.QtGui import *
from PySide.QtCore import *
from ui_mainwindow import Ui_MainWindow

import cv2
import rosbag
import numpy as np
import os
import tempfile

import visualizationLogs
import images
import subprocess
import shutil

class Viewer(QMainWindow, Ui_MainWindow):
    def __init__(self, parent = None):
        super(Viewer, self).__init__(parent)
        self.setupUi(self)

        # Set up icons
        self.actionLoad_dataset.setIcon(self.style().standardIcon(QStyle.SP_DialogOpenButton))

        # Set up image grid
        gridW = 5
        gridH = 3
        self.imageWidgets = [ None ] * gridH
        self.imageLabels = [ None ] * gridH
        for row in xrange(gridH):
            self.imageWidgets[row] = [ None ] * gridW
            self.imageLabels[row] = [ None ] * gridW
            for col in xrange(gridW):
                image = QLabel()
                image.setFrameShape(QFrame.Shape.Box)
                image.setScaledContents(True)
                image.setSizePolicy(QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed, QSizePolicy.DefaultType))
                def resizeLabel(label):
                    label.setMinimumWidth(10)
                    label.sizeHint = lambda: QSize(1000, 1000)
                    label.setMinimumHeight(self.aspectRatio * label.width())
                    label.setMaximumHeight(self.aspectRatio * label.width())
                image.resizeEvent = (lambda img: (lambda event: resizeLabel(img)))(image)
                label = QLabel()
                label.setSizePolicy(QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Minimum, QSizePolicy.DefaultType))
                self.imageWidgets[row][col] = image
                self.imageLabels[row][col] = label
                layout = QVBoxLayout()
                layout.addWidget(image)
                layout.addWidget(label)
                self.imageGrid.addLayout(layout, row, col)
        
        # Variables to be used later
        self.nTransBases = 1
        self.aspectRatio = 1.0
        
        # Load dataset if requested
        try:
            p = sys.argv.index('-d')
            self.loadDataset(sys.argv[p+1])
        except ValueError:
            pass
        
    @Slot()
    def on_actionLoad_dataset_triggered(self):
        newResultsBag = QFileDialog.getOpenFileName(self, "Choose results bag file", "", "*.bag")
        if newResultsBag[0] != "":
            self.loadDataset(newResultsBag[0])
        
    def loadDataset(self, bagfile):
        # Open bag file
        self.resultsBag = rosbag.Bag(bagfile)
        self.setWindowTitle(bagfile)
        
        # Index entries
        self.bagIndices = visualizationLogs.countEntries(self.resultsBag)
        self.sortedIterations = sorted(self.bagIndices['allIterations'])
        self.sortedFrames = sorted(self.bagIndices['allFrames'])
        
        # Update GUI
        self.imageWidgets[len(self.imageWidgets)-1][len(self.imageWidgets[0])-1].setPixmap(images.pixmapMakeColorWheel(100))
        self.updateControlLimits()
        self.loadData()
            
    @Slot()
    def on_actionExport_movie_triggered(self):
        # Get output directory
        dirToWrite = QFileDialog.getSaveFileName(self, 'Enter directory to create')
        if dirToWrite[0] == '': return
        os.mkdir(dirToWrite[0])
        
        for frame in xrange(self.frameSlider.minimum(), self.frameSlider.maximum() + 1):
            # Set frame
            self.frameSlider.setValue(frame)
            self.loadData()
            QApplication.processEvents()
            
            # Add movie frame
            pixmap = QPixmap.grabWidget(self.centralwidget)
            pixmap.save('%s/%08d.jpg' % (dirToWrite[0], frame))
            
    @Slot()
    def on_actionExport_overlay_movie_triggered(self):
      # Get movie file to save
      movieFile = QFileDialog.getSaveFileName(self, 'Enter movie file name', '', '*.mp4')[0]
      if len(movieFile) == 0:
        return
      
      # Create temporary directory for images
      tmpdir = tempfile.mkdtemp()
      try:
        
        # Iterate over frames
        image_number = 0;
        iteration = self.sortedIterations[self.iterationSlider.value()]
        for framen in xrange(self.frameSlider.minimum(), self.frameSlider.maximum() + 1):
          frame = self.sortedFrames[framen]
          msgs = visualizationLogs.getMessages(self.resultsBag, self.bagIndices, iteration, frame)
          for msg in msgs:
            if msg._type == 'generalflow_rosmsgs/FullImageErrorVisualization':
              # Load image and labels
              image = images.arrayFromImgMsg(msg.prevFrame)
              
              if image.dtype == np.float32 or image.dtype == np.float64:
                image = (255.0 * image).astype(np.uint8)
              if len(image.shape) == 2:
                image = image[:, :, np.newaxis]
              if image.shape[2] == 1:
                image = np.repeat(image, 3, 2)
              
              indicators = images.arrayFromImgMsg(msg.indicators)
              nComponents = msg.indicators.height / msg.curFrame.height
              if nComponents == 3:
                mask = indicators[0::3, :]
              elif nComponents == 4:
                mask = indicators[0::4, :] + indicators[2::4, :]
              else:
                raise RuntimeError('Only know how to deal with 4 indicator values')
              
              if mask.dtype == np.float32 or mask.dtype == np.float64:
                mask = (255.0 * mask).astype(np.uint8)
              
              # Create overlay
              images.tintImage(image, mask)
              # Write frame
              cv2.imwrite(os.path.join(tmpdir, '%08d.jpg' % image_number), image)
              
              print 'Frame %d' % frame
              image_number += 1
        # Build movie
        subprocess.check_call(('ffmpeg',
                               '-i', os.path.join(tmpdir, '%08d.jpg'),
                               '-b:v', '1000000',
                               movieFile))
      finally:
        shutil.rmtree(tmpdir)

    def updateControlLimits(self):
        self.iterationSlider.setRange(0, len(self.sortedIterations) - 1)
        self.frameSlider.setRange(0, len(self.sortedFrames) - 1)

    @Slot(int)
    def on_iterationSlider_valueChanged(self, iterationIndex):
        self.loadData()

    @Slot(int)
    def on_frameSlider_valueChanged(self, frameIndex):
        self.loadData()

    def loadData(self):
        self.iterationLabel.setNum(self.sortedIterations[self.iterationSlider.value()])
        self.frameLabel.setNum(self.sortedFrames[self.frameSlider.value()])        
        
        iteration = self.sortedIterations[self.iterationSlider.value()]
        frame = self.sortedFrames[self.frameSlider.value()]
        msgs = visualizationLogs.getMessages(self.resultsBag, self.bagIndices, iteration, frame)

        haveFullImageErrorVisualization = False
        haveBasisUpdate = False
        for msg in msgs:
            if msg._type == 'generalflow_rosmsgs/FullImageErrorVisualization':
                haveFullImageErrorVisualization = True
                
                self.imageWidgets[0][0].setPixmap(
                    images.pixmapFromArray(images.arrayFromImgMsg(msg.prevFrame)))
                self.imageLabels[0][0].setText('prevFrame')

                self.imageWidgets[1][0].setPixmap(
                    images.pixmapFromArray(images.arrayFromImgMsg(msg.curFrame)))
                self.imageLabels[1][0].setText('curFrame')
                
                self.aspectRatio = float(msg.curFrame.height) / msg.curFrame.width
                
                temporalDeriv, temporalDerivMax = images.pixmapFromPosNegCvImage(images.cvImageFromImgMsg(msg.temporalDeriv))
                self.imageWidgets[2][0].setPixmap(temporalDeriv)
                self.imageLabels[2][0].setText('temporalDeriv\nmax value = %g' % temporalDerivMax)
                    
                invSigmas, invSigmasMax = images.pixmapFromPosNegCvImage(
                                            images.cvImageFromImgMsg(msg.invSigmas),
                                            msg.invSigmas.height / msg.curFrame.height)
                self.imageWidgets[0][1].setPixmap(invSigmas)
                self.imageLabels[0][1].setText('invSigmas\nmax value = %g' % invSigmasMax)
                
                errs, errsMax = images.pixmapFromPosNegCvImage(images.cvImageFromImgMsg(msg.errs), msg.errs.height / msg.curFrame.height)
                self.imageWidgets[1][1].setPixmap(errs)
                self.imageLabels[1][1].setText('errs\nmax value = %g' % errsMax)
                
                normalizedErrs, normalizedErrsMax = images.pixmapFromPosNegCvImage(
                                                    images.cvImageFromImgMsg(msg.normalizedErrs),
                                                    msg.normalizedErrs.height / msg.curFrame.height)
                self.imageWidgets[2][1].setPixmap(normalizedErrs)
                self.imageLabels[2][1].setText('normalizedErrs\nmax value = %g' % normalizedErrsMax)
                    
                nComponents = msg.indicators.height / msg.curFrame.height

                if nComponents == 2:
                    rgbMapping = [ 0, None, 1 ]
                elif nComponents == 3:
                    rgbMapping = [ 0, 1, 2 ]
                elif nComponents == 4:
                    rgbMapping = [ [0, 2], 1, [3, 2] ]
                else:
                    raise RuntimeError('Unexpected size of indicator image')

                if hasattr(msg, 'indicatorEvidence'):
#                     evidence = images.arrayFromImgMsg(msg.indicatorEvidence)
#                     evidence = evidence.reshape([evidence.shape[0] / 4, evidence.shape[1], 4])
#                     maxlabel = np.argmax(evidence, axis = 2)
#                     evidence = np.zeros_like(evidence)
#                     for y in xrange(maxlabel.shape[0]):
#                         for x in xrange(maxlabel.shape[1]):
#                             evidence[y, x, maxlabel[y, x]] = 1.0
#                     evidence = evidence.reshape([evidence.shape[0] * 4, evidence.shape[1]])
#                     self.imageWidgets[0][2].setPixmap(images.pixmapFromIndicators(images.cvImageFromArray(evidence), rgbMapping))
                    
                    array = images.arrayFromImgMsg(msg.indicatorEvidence)
                    array = np.subtract(array, np.amin(array))
                    array = np.divide(array, np.amax(array))
                    cvImage = images.cvImageFromArray(array)
                    self.imageWidgets[0][2].setPixmap(images.pixmapFromIndicators(cvImage, nComponents, rgbMapping))
                    self.imageLabels[0][2].setText('indicatorEvidence')
                    
                    #indicatorSmoothness, indicatorSmoothnessMax = images.pixmapFromOpticalFlow(images.cvImageFromImgMsg(msg.indicatorSmoothness))
                    #self.imageWidgets[1][2].setPixmap(indicatorSmoothness)
                    #self.imageLabels[1][2].setText('indicatorSmoothness\nmax value = %g' % indicatorSmoothnessMax)

                self.imageWidgets[2][2].setPixmap(
                    images.pixmapFromIndicators(images.cvImageFromImgMsg(msg.indicators), nComponents, rgbMapping))
                self.imageLabels[2][2].setText('indicators')
                
                expectedFlow, flowMax = images.pixmapFromOpticalFlow(images.cvImageFromImgMsg(msg.expectedFlow))
                self.imageWidgets[0][4].setPixmap(expectedFlow)
                self.imageLabels[0][4].setText('expectedFlow\nmax value = %g' % flowMax)
            elif msg._type == 'generalflow_rosmsgs/BasisUpdate':
                haveBasisUpdate = True
                
                basisRot, rotMax = images.pixmapFromBasis(msg.basisRot, msg.imgWidth, msg.imgHeight)
                self.imageWidgets[0][3].setPixmap(basisRot)
                self.imageLabels[0][3].setText('basisRot\nmax value = %g' % rotMax)

                self.aspectRatio = float(msg.imgHeight) / msg.imgWidth

                row = 1
                col = 3
                for k in xrange(len(msg.basesTrans)):
                    basisTrans, transMax = images.pixmapFromBasis(msg.basesTrans[k], msg.imgWidth, msg.imgHeight)
                    self.imageWidgets[row][col].setPixmap(basisTrans)
                    self.imageLabels[row][col].setText('basesTrans[%d]\nmax value = %g' % (k, transMax))
                    row = row + 1
                    if row == len(self.imageWidgets):
                        row = 0
                        col = col + 1
                
        if not haveFullImageErrorVisualization:
            self.imageWidgets[0][0].setPixmap(QPixmap())
            self.imageWidgets[1][0].setPixmap(QPixmap())
            self.imageWidgets[2][0].setPixmap(QPixmap())
            self.imageWidgets[0][1].setPixmap(QPixmap())
            self.imageWidgets[1][1].setPixmap(QPixmap())
            self.imageWidgets[2][1].setPixmap(QPixmap())
            self.imageWidgets[0][2].setPixmap(QPixmap())
            self.imageWidgets[1][2].setPixmap(QPixmap())
            self.imageWidgets[2][2].setPixmap(QPixmap())
            self.imageWidgets[0][4].setPixmap(QPixmap())
            
        if not haveBasisUpdate:
            self.imageWidgets[0][3].setPixmap(QPixmap())
            self.imageLabels[0][3].setText('')
            row = 1
            col = 3
            for k in xrange(self.nTransBases):
                self.imageWidgets[row][col].setPixmap(QPixmap())
                self.imageLabels[row][col].setText('')
                row = row + 1
                if row == len(self.imageWidgets):
                    row = 0
                    col = col + 1
        
        # Get velocity
        if(iteration in self.bagIndices['velocityIterationFrameIndex']
           and frame in self.bagIndices['velocityIterationFrameIndex'][iteration]):
            velocity = self.bagIndices['velocityIterationFrameIndex'][iteration][frame]
            self.velocityLabel.setText('Velocity: %r' % list(velocity))
        else:
            self.velocityLabel.setText('')

if __name__ == '__main__':
    app = QApplication(sys.argv)
    frame = Viewer()
    frame.show()
    app.exec_()
