'''
Created on Jan 8, 2014

@author: richard
'''

import sensor_msgs.msg
import cv2
import images
import os
import PySide.QtGui as q
import PySide.QtCore

def countEntries(bag, nthFrame):
    class FrameIndexEntry():
        def __init__(self, logTime, msgTime):
            self.logTime = logTime
            self.msgTime = msgTime
            
    frameIndex = []
    framenum = 1
    
    for topic, msg, t in bag.read_messages():
        if msg._type == sensor_msgs.msg.Image._type:
            if framenum % nthFrame == 0:
                frameIndex.append(FrameIndexEntry(t, msg.header.stamp))
            framenum = framenum + 1
            
    return dict( { 'frameIndex': frameIndex } )

def getMessages(bag, indices, frame):
    msgs = []
    timestamp = indices['frameIndex'][frame].logTime
    for topic, msg, t in bag.read_messages(start_time = timestamp, end_time = timestamp):
        msgs.append(msg)
    return msgs
    
def getLabelingDir(parentDir, bag):
    bagname = os.path.splitext(os.path.basename(bag.filename))[0]
    outputPath = '%s/labeling_%s' % (parentDir, bagname)
    return outputPath

def exportSegment(mainWindow, bag, indices, startFrame, endFrame, directory, nthFrame):
    doExport = True
    try:
        outputPath = getLabelingDir(directory, bag)
        os.mkdir(outputPath)
    except OSError:
        answer = q.QMessageBox.question(mainWindow, 'Output directory exists',
                      'The output directory \'%s\' already exists.  Do you want to export images into this directory anyway, overwriting any existing images with the same timestamps?' % outputPath,
                      q.QMessageBox.Yes | q.QMessageBox.Cancel)
        if answer != q.QMessageBox.Yes:
            doExport = False
            
    if doExport:
        progress = q.QProgressDialog('Exporting images', 'Cancel', 0, endFrame - startFrame + 1, mainWindow)
        try:
            filesFile = open('/home/richard/src/InteractLabeler1_2_1/DefaultTimeLineFile_unix.txt', 'w')
            progress.setWindowModality(PySide.QtCore.Qt.WindowModal)
            nExported = 0
            frameNum = 0 # Starts at 0 because startFrame is already the first real frame
            for topic, msg, t in bag.read_messages(start_time = indices['frameIndex'][startFrame].logTime,
                                                   end_time = indices['frameIndex'][endFrame].logTime):
                if msg._type == sensor_msgs.msg.Image._type:
                    print 'Message time %.6f' % msg.header.stamp.to_sec()
                    if frameNum % nthFrame == 0:
                        img = images.arrayFromImgMsg(msg)
                        img = cv2.cvtColor(img, cv2.cv.CV_GRAY2BGR)
                        filename = '%s/%.6f.png' % (outputPath, msg.header.stamp.to_sec())
                        cv2.imwrite(filename, img)
                        filesFile.write(filename + '\n')
                        nExported = nExported + 1
                        if nExported % 10 == 0:
                            progress.setValue(nExported)
                    q.QApplication.processEvents()
                    if progress.wasCanceled():
                        return ('Cancelled', outputPath)
                    frameNum = frameNum + 1
        except:
            return ('Error', outputPath)
        finally:
            filesFile.close()
            progress.close()
            
    return ('Ok', outputPath)
    
def exportImage(bag, indices, frame, directory):
    for topic, msg, t in bag.read_messages(start_time = indices['frameIndex'][frame].logTime,
                                           end_time = indices['frameIndex'][frame].logTime):
        if msg._type == sensor_msgs.msg.Image._type:
            img = images.arrayFromImgMsg(msg)
            img = cv2.cvtColor(img, cv2.cv.CV_GRAY2BGR)
            filename = '%s/%.6f.png' % (directory, msg.header.stamp.to_sec())
            cv2.imwrite(filename, img)
            return filename
    raise RuntimeError('Could not find the image in the bag file')
