from PySide.QtGui import QPixmap, QImage
import cv2
import numpy as np
import cv_bridge

cvbridge = cv_bridge.CvBridge()

def arrayFromCvImage(cvImage):
    singleChannelType = cvImage.type & 0x7
    if   singleChannelType == cv2.CV_8U : npType = np.uint8
    elif singleChannelType == cv2.CV_8S : npType = np.int8
    elif singleChannelType == cv2.CV_16U: npType = np.uint16
    elif singleChannelType == cv2.CV_16S: npType = np.int16
    elif singleChannelType == cv2.CV_32F: npType = np.float32
    elif singleChannelType == cv2.CV_64F: npType = np.float64
    else: raise RuntimeError('Unknown OpenCV image type')
    
    array = np.ndarray(shape = [ cvImage.rows, cvImage.cols,
                                cvImage.channels ],
                       dtype = npType, order = 'C',
                       buffer = cvImage.tostring())
    return array

def cvImageFromArray(array):
    if   array.dtype == np.uint8  : singleChannelType = cv2.CV_8U
    elif array.dtype == np.int8   : singleChannelType = cv2.CV_8S
    elif array.dtype == np.uint16 : singleChannelType = cv2.CV_16U
    elif array.dtype == np.int16  : singleChannelType = cv2.CV_16S
    elif array.dtype == np.float32: singleChannelType = cv2.CV_32F
    elif array.dtype == np.float64: singleChannelType = cv2.CV_64F
    else: raise RuntimeError('Unknown array type')
    
    if len(array.shape) == 3:
        cvType = singleChannelType | ((array.shape[2] - 1) << 3)
    elif len(array.shape) == 2:
        cvType = singleChannelType
    else:
        raise RuntimeError('Unsupported number of array dimensions')
    
    cvImage = cv2.cv.CreateMatHeader(array.shape[0], array.shape[1], cvType)
    data = array.tostring(order = 'C')
    cv2.cv.SetData(cvImage, data)
    
    return cvImage

def cvImageFromImgMsg(imgMsg):
    return cvbridge.imgmsg_to_cv(imgMsg)

def arrayFromImgMsg(imgMsg):
    return cvbridge.imgmsg_to_cv2(imgMsg)
    
def imgMsgFromCvImage(cvImage):
    return cvbridge.cv_to_imgmsg(cvImage)
  
def pixmapFromArray(array):
  # Convert to 8-bit
  if array.dtype == np.float32 or array.dtype == np.float64:
    as8bit = (array * 255.0).astype(np.uint8)
  elif array.dtype == np.uint8 or array.dtype == np.int8:
    as8bit = array
  
  if as8bit.shape[2] == 1:
    as8bit = np.repeat(as8bit, 3, 2)
    
  # Note .copy() to avoid data sharing
  # Note rgbSwapped()
  return QPixmap.fromImage(QImage(as8bit.data, as8bit.shape[1], as8bit.shape[0],
                                  QImage.Format.Format_RGB888).rgbSwapped().copy())

def pixmapFromPosNegCvImage(cvImage, nImagesPacked = 1):
    if cvImage.channels != 1: raise RuntimeError('Need single-channel image for pixmapFromPosNegCvImage')
    
    # Build channels separately
    singleChannelType = cvImage.type & 0x7
    r = cv2.cv.CreateMat(cvImage.rows, cvImage.cols, singleChannelType)
    cv2.cv.Copy(cvImage, r)
    g = cv2.cv.CreateMat(cvImage.rows, cvImage.cols, singleChannelType)
    cv2.cv.Copy(cvImage, g)
    b = cv2.cv.CreateMat(cvImage.rows, cvImage.cols, singleChannelType)
    cv2.cv.Set(b, 0.0)
    
    cv2.cv.Threshold(r, r, 0.0, 0.0, cv2.cv.CV_THRESH_TOZERO_INV)
    cv2.cv.Scale(r, r, -1.0)
    
    cv2.cv.Threshold(g, g, 0.0, 0.0, cv2.cv.CV_THRESH_TOZERO)
    
    # Combine channels
    rgbImage = cv2.cv.CreateMat(cvImage.rows, cvImage.cols, singleChannelType | (2 << 3))
    cv2.cv.Merge(r, g, b, None, rgbImage)
    
    # Get maximum value and scale
    maxValue = max(cv2.cv.MinMaxLoc(r)[1], cv2.cv.MinMaxLoc(g)[1])
    cv2.cv.Scale(rgbImage, rgbImage, 1.0 / maxValue)

    # Convert to numpy image
    if cvImage.type == cv2.CV_32F: npType = np.float32
    elif cvImage.type == cv2.CV_64F: npType = np.float64
    else: raise RuntimeError('Need 32 or 64 bit float for pixmapFromPosNegCvImage')
    data = rgbImage.tostring()
    rgb = np.ndarray([rgbImage.rows, rgbImage.cols, 3], dtype = npType, buffer = data, order = 'C')
    
    # Tile
    imgFunction = lambda i: (rgb[i::nImagesPacked, :, :] * 255.0).astype(np.uint8)
    image = createTiledImage(imgFunction, nImagesPacked, 3, cvImage.cols, cvImage.rows / nImagesPacked)
    
    # Note .copy() to avoid data sharing
    return QPixmap.fromImage(QImage(image.data, image.shape[1], image.shape[0],
                                    QImage.Format.Format_RGB888).copy()), maxValue

    
def colorOpticalFlow(flow):
    X = flow[0::2, :]
    Y = flow[1::2, :]

    # Note -Y so that the direction matches the standard HSV color wheel 
    mag, ang = cv2.cartToPolar(X, -Y)
    hsv = np.ndarray(shape = [flow.shape[0] / 2, flow.shape[1], 3], dtype = np.uint8,
                     order = 'C')
    hsv[...,0] = ang * 180.0 / np.pi / 2.0
    hsv[...,2] = 255
    hsv[...,1] = mag * 255.0
    return cv2.cvtColor(hsv, cv2.COLOR_HSV2RGB)

def createTiledImage(imgFunction, nImgs, nChannels, imgWidth, imgHeight):
    gridrows = int(np.sqrt(nImgs))
    if gridrows * gridrows == nImgs:
        gridcols = gridrows
    else:
        gridrows = gridrows + 1
        gridcols = gridrows
    
    image = np.zeros(shape = [imgHeight * gridrows, imgWidth * gridcols, nChannels], dtype = np.uint8, order = 'C')
    
    for y in xrange(gridrows):
        for x in xrange(gridcols):
            if x + y*gridcols < nImgs:
                image[(y*imgHeight):((y+1)*imgHeight), (x*imgWidth):((x+1)*imgWidth), :] = imgFunction(x + y*gridcols)
    return image

def pixmapMakeColorWheel(size):
    X = np.repeat(np.linspace(-1.0, 1.0, size)[np.newaxis, :], size, 0)
    Y = np.repeat(np.linspace(-1.0, 1.0, size)[:, np.newaxis], size, 1)
    
    flow = np.ndarray([size*2, size], dtype = X.dtype)
    flow[0::2, :] = X
    flow[1::2, :] = Y
    
    rgb = colorOpticalFlow(flow)
    return QPixmap.fromImage(QImage(rgb.data, rgb.shape[1], rgb.shape[0],
                            QImage.Format.Format_RGB888).copy())

def pixmapFromOpticalFlow(cvImage):
    if cvImage.channels != 1: raise RuntimeError('Need single-channel image for colorOpticalFlow')
    if cvImage.type == cv2.CV_32F: npType = np.float32
    elif cvImage.type == cv2.CV_64F: npType = np.float64
    else: raise RuntimeError('Need 32 or 64 bit float for colorOpticalFlow')
    data = cvImage.tostring()
    flow = np.ndarray(shape = [cvImage.rows, cvImage.cols], dtype = npType,
                      buffer = data, order = 'C')
    
    # Get maximum value and scale
    X = flow[0::2, :]
    Y = flow[1::2, :]
    maxValue = np.max(np.sqrt(np.square(X) + np.square(Y)))
    flow = flow * (1.0 / maxValue)

    rgb = colorOpticalFlow(flow)

    # Note .copy() to avoid data sharing
    data = rgb.data
    return QPixmap.fromImage(QImage(rgb.data, rgb.shape[1], rgb.shape[0],
                                    QImage.Format.Format_RGB888).copy()), maxValue
    
def pixmapFromBasis(basisMsg, imgWidth, imgHeight):
    if imgWidth * imgHeight * 2 != basisMsg.rows: raise RuntimeError('Image dimensions do not match basis')
    basis = np.ndarray(shape = [basisMsg.rows, basisMsg.cols], buffer = np.array(basisMsg.coeffs), order = 'F')
    
    q = basis.shape[1]
    
    # Get maximum value and scale
    X = basis[0::2, :]
    Y = basis[1::2, :]
    maxValue = np.max(np.sqrt(np.square(X) + np.square(Y)))
    basis = basis * (1.0 / maxValue)
    
    imgFunction = lambda i: colorOpticalFlow(basis[:, i].reshape([imgHeight * 2, imgWidth], order='F'))
    image = createTiledImage(imgFunction, q, 3, imgWidth, imgHeight)

    # Note .copy() to avoid data sharing
    return QPixmap.fromImage(QImage(image.data, image.shape[1], image.shape[0],
                                    QImage.Format.Format_RGB888).copy()), maxValue
                                    
def pixmapFromIndicators(cvImage, nComponents, rgbMapping = [ 0, 1, 2 ]):
    if cvImage.channels != 1:
        raise RuntimeError('Need single-channel image in pixmapFromIndicators')
    if cvImage.type == cv2.CV_32F: npType = np.float32
    elif cvImage.type == cv2.CV_64F: npType = np.float64
    else: raise RuntimeError('Need 32 or 64 bit float for pixmapFromIndicators')
    
    # Get image in numpy format
    data = cvImage.tostring()
    indicators = np.ndarray([cvImage.rows, cvImage.cols], dtype = npType, buffer = data, order = 'C')
    
    # Check range
    if not (np.all(np.logical_and(np.greater_equal(indicators, 0.0), np.less_equal(indicators, 1.0)))):
        print 'Out of range value in pixmapFromIndicators: %r' % indicators[np.argwhere(np.logical_not(np.logical_and(np.greater_equal(indicators, 0.0),
                                                                                                                      np.less_equal(indicators, 1.0))))]
    
    # Put lines into rgb mapping
    actualHeight = cvImage.rows / nComponents
    if actualHeight * nComponents != cvImage.rows: raise RuntimeError('Number of components not consistent with image size in pixmapFromIndicators')
    if len(rgbMapping) != 3: raise RuntimeError('rgbMapping must be of length 3')
    rgb = np.zeros([actualHeight, cvImage.cols, 3], dtype = np.uint8)
    
    for c in xrange(3):
        if rgbMapping[c] != None:
            if hasattr(rgbMapping[c], '__iter__'):
                for cc in rgbMapping[c]:
                    rgb[:, :, c] = (rgb[:, :, c] +
                                   indicators[cc::nComponents, :] * 255.0)
            else:
                rgb[:, :, c] = indicators[(rgbMapping[c])::nComponents, :] * 255
    
    # Note .copy() to avoid data sharing
    return QPixmap.fromImage(QImage(rgb.data, rgb.shape[1], rgb.shape[0],
                                    QImage.Format.Format_RGB888).copy())

def shadeImage(img, mask):
    shade = np.ones(img.shape[0:2], np.uint8) * 50
    shade = shade + mask * (255-50)
    return img[:, :, 0] * shade

def tintImage(img, mask):
  # Apply mask
  if len(mask.shape) == 3:
    mask = mask[:,:,0]
  dif = 255 - img[:, :, 2]
  toadd = (dif * (mask.astype(np.float32) / 255.0)).astype(np.uint8)
  img[:, :, 2] = img[:, :, 2] + toadd
