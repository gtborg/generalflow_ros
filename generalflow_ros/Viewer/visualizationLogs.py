import generalflow_rosmsgs.msg as rosmsgs

def _iterationIndexAddFrame(iterationIndex, iteration, frame, timestamp):
    if iteration in iterationIndex:
        if frame in iterationIndex[iteration]:
            iterationIndex[iteration][frame].append(timestamp)
        else:
            iterationIndex[iteration][frame] = [ timestamp ]
    else:
        iterationIndex[iteration] = dict( { frame: [ timestamp ] } )

def _iterationIndexAddUpdate(iterationIndex, iteration, timestamp):
    if iteration in iterationIndex:
        iterationIndex[iteration].append(timestamp)
    else:
        iterationIndex[iteration] = [ timestamp ]

def _velocityIterationFrameAdd(iterationIndex, iteration, frame, velocity):
    if iteration in iterationIndex:
        if frame in iterationIndex[iteration]:
            raise RuntimeError('Duplicate velocity entry found')
        else:
            iterationIndex[iteration][frame] = velocity
    else:
        iterationIndex[iteration] = dict( { frame: velocity } )
        
def _resultsTimestampAdd(resultsTimestampIndex, imgTime, bagTime):
    resultsTimestampIndex[imgTime] = bagTime

def countEntries(bag):
    # Sets of iterations and frames to return
    iterationFrameIndex = dict()
    iterationUpdateIndex = dict()
    velocityIterationFrameIndex = dict()
    resultsTimestampIndex = dict()
    allIterations = set()
    allFrames = set()

    for topic, msg, t in bag.read_messages():
        if msg._type == rosmsgs.FullImageErrorVisualization._type:
            _iterationIndexAddFrame(iterationFrameIndex, msg.iteration, msg.framenum, t)
            _resultsTimestampAdd(resultsTimestampIndex, msg.header.stamp, t)
            allIterations.add(msg.iteration)
            allFrames.add(msg.framenum)
        elif msg._type == rosmsgs.BasisUpdate._type:
            _iterationIndexAddUpdate(iterationUpdateIndex, msg.iteration, t)
            allIterations.add(msg.iteration)
        elif msg._type == rosmsgs.VelocityUpdate._type:
            for velocityFrame in msg.velocities:
                _velocityIterationFrameAdd(velocityIterationFrameIndex, msg.iteration,
                                           velocityFrame.frame, velocityFrame.velocity)
                allFrames.add(velocityFrame.frame)
            allIterations.add(msg.iteration)

    result = dict({
                   'iterationFrameIndex': iterationFrameIndex,
                   'iterationUpdateIndex': iterationUpdateIndex,
                   'velocityIterationFrameIndex': velocityIterationFrameIndex,
                   'resultsTimestampIndex': resultsTimestampIndex,
                   'allIterations': allIterations,
                   'allFrames': allFrames})
    return result;

def getMessages(bag, indices, iteration, frame):
    timestamps = list()

    if(iteration in indices['iterationFrameIndex']
       and frame in indices['iterationFrameIndex'][iteration]):
        timestamps.extend(indices['iterationFrameIndex'][iteration][frame])

    if(iteration in indices['iterationUpdateIndex']):
        timestamps.extend(indices['iterationUpdateIndex'][iteration])

    msgs = []
    for timestamp in timestamps:
        for topic, msg, t in bag.read_messages(start_time = timestamp, end_time = timestamp):
            if(msg._type == rosmsgs.FullImageErrorVisualization._type
               or msg._type == rosmsgs.BasisUpdate._type
               or msg._type == rosmsgs.VelocityUpdate._type):
                msgs.append(msg)

    return msgs


    