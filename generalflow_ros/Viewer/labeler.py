#!/usr/bin/python

'''
Created on Jan 7, 2014

@author: richard
'''

import sys
import subprocess
import shutil
import os

import PySide.QtGui as q
import PySide.QtCore
from labeler_mainwindow import Ui_labeler_MainWindow
import robotLogs
import images
import visualizationLogs
import generalflow_rosmsgs.msg

import rosbag
import sensor_msgs.msg
import cv2
import numpy as np

nthFrame = 5

class Labeler(q.QMainWindow, Ui_labeler_MainWindow):
    def __init__(self, parent = None):
        super(Labeler, self).__init__(parent)
        self.setupUi(self)
        
        self.currentImage.setScaledContents(True)
        self.currentImage.setSizePolicy(q.QSizePolicy(q.QSizePolicy.Fixed, q.QSizePolicy.Expanding, q.QSizePolicy.Label))
        def resizeLabel(label):
            label.setMinimumWidth(label.height())
            label.setMaximumWidth(label.height())
        self.currentImage.resizeEvent = (lambda img: (lambda event: resizeLabel(img)))(self.currentImage)
    
    @PySide.QtCore.Slot()    
    def on_actionOpen_robot_log_triggered(self):
        newLogBag = q.QFileDialog.getOpenFileName(self, "Choose data bag file", "", "*.bag")
        if newLogBag[0] != "":
            # Open bag file
            self.logBag = rosbag.Bag(newLogBag[0])
            self.setWindowTitle(newLogBag[0])
            
            # Index entries
            self.indices = robotLogs.countEntries(self.logBag, nthFrame)
            self.updateControlLimits()
            self.loadData()
                        
    @PySide.QtCore.Slot()
    def on_actionLabel_segment_triggered(self):
        if self.segmentStart > self.segmentEnd:
            q.QMessageBox.information(0, 'Nothing to export', 'The segment end time is not greater than the segment start time, so there is nothing to export')
        else:
            directory = q.QFileDialog.getExistingDirectory(self, 'Choose parent directory of new directory for exported images')
            if directory != '':
                result = robotLogs.exportSegment(self, self.logBag, self.indices, self.segmentStart, self.segmentEnd, directory, nthFrame)
                if result[0] == 'Ok':
                    self.warningLabel.setText('<span style=" color:#00ff00;">Successfully exported into %s</span>' % result[1])
                elif result[0] == 'Cancelled':
                    self.warningLabel.setText('<span style=" color:#ff00ff;">Cancelled export into %s</span>' % result[1])
                    return
                elif result[0] == 'Error':
                    self.warningLabel.setText('<span style=" color:#ff0000;">Error exporting into %s</span>' % result[1])
                    return
                
                # Convert paths to wine paths
                p = subprocess.Popen('winepath `cat DefaultTimeLineFile_unix.txt` > DefaultTimeLineFile.txt',
                                     shell=True, cwd='/home/richard/src/InteractLabeler1_2_1')
                p.wait()
                
                # Run labeler
                p = subprocess.Popen(['wine','InteractLabeler1_2_1.exe','Default.meta'],
                                     cwd='/home/richard/src/InteractLabeler1_2_1')
                p.wait()
                                     
                # Copy labeled images to the output directory
                imgs = open('/home/richard/src/InteractLabeler1_2_1/DefaultTimeLineFile_unix.txt', 'r').read().splitlines()
                inname, inext = os.path.splitext(self.logBag.filename)
                for i in xrange(sys.maxint):
                    bagfilename = '%s_labeled_%d%s' % (inname, i, inext)
                    if not os.path.isfile(bagfilename): break
                bag = rosbag.Bag(bagfilename, mode='w')
                relativeFrameNum = 0
                for img in imgs:
                    filename = os.path.basename(img)
                    os.remove(img)
                    print 'Deleting %s' % img
                    name, ext = os.path.splitext(filename)
                    labeledName = '/home/richard/src/InteractLabeler1_2_1/Labels/' + name + '_L' + ext
                    if os.path.isfile(labeledName):
                        msg = images.imgMsgFromCvImage(images.cvImageFromArray(cv2.imread(labeledName)))
                        msg.header.stamp = self.indices['frameIndex'][self.segmentStart + relativeFrameNum].msgTime
                        logTime = self.indices['frameIndex'][self.segmentStart + relativeFrameNum].logTime
                        print 'Storing %s with message time %.6f' % (labeledName, msg.header.stamp.to_sec())
                        bag.write('/labeler/frames', msg, logTime)
                        #shutil.copy(labeledName, result[1])
                    relativeFrameNum = relativeFrameNum + 1
                bag.close()
                
    @PySide.QtCore.Slot()
    def on_actionAnalyze_labels_triggered(self):
        evaluationFile = q.QFileDialog.getOpenFileName(self, "Choose evaluation results bag file", "", "*.bag")
        if evaluationFile[0] == '': return
        evaluationFile = evaluationFile[0]
    
        # Open output file
        bagname = os.path.splitext(evaluationFile)[0]
        outputFilename = '%s_analysis.txt' % bagname
        outputDirectory = '%s_analysis' % bagname

        # Check if output file exists
        if os.path.isfile(outputFilename) and q.QMessageBox.Cancel == q.QMessageBox.question(self,
                             'Overwrite analysis file?',
                             'The output file %s exists, do you want to overwrite it?' % outputFilename,
                             q.QMessageBox.Yes | q.QMessageBox.Cancel):
            return
    
        if os.path.isdir(outputDirectory):
            shutil.rmtree(outputDirectory)
        elif os.path.exists(outputDirectory):
            q.QMessageBox.critical(self, 'Labeler', 'Output directory name exists as a file but is not a directory')
            return
        print 'Opening output file %s' % outputFilename
        os.mkdir(outputDirectory)
        outputFile = open(outputFilename, 'w')
        
        # Read labels
        labelBags = []
        inname, inext = os.path.splitext(self.logBag.filename)
        for i in xrange(sys.maxint):
            bagfilename = '%s_labeled_%d%s' % (inname, i, inext)
            if not os.path.isfile(bagfilename): break
            else:
                print 'Opening %s' % bagfilename
                labelBags.append(rosbag.Bag(bagfilename))
            
        # Index labels
        labelTimes = {}
        bagIndex = 0
        for bag in labelBags:
            for topic, msg, t in bag.read_messages():
                if msg._type == sensor_msgs.msg.Image._type:
                    if msg.header.stamp in labelTimes:
                        outputFile.write('Warning: Duplicate label for image timestamp %.6f\n' % msg.header.stamp.to_sec())
                    labelTimes[msg.header.stamp] = (bagIndex, t)
            bagIndex = bagIndex + 1

        # Read evaluation file
        evaluationBag = rosbag.Bag(evaluationFile)
        evaluationIndices = visualizationLogs.countEntries(evaluationBag)
        
        # Initialize sums
        totalFalseNegatives = 0
        totalFalsePositivesWout = 0
        totalFalsePositivesWith = 0
        totalTrueNegatives = 0
        totalTruePositivesWout = 0
        totalTruePositivesWith = 0
        totalTruthPositives = 0
        totalTruthNegatives = 0
        
        # Loop over times in segment
        for frame in xrange(self.segmentStart, self.segmentEnd + 1):
            # Get image timestamp
            imgTime = self.indices['frameIndex'][frame].msgTime
            
            # Check for evaluation results at this timestamp
            if imgTime in evaluationIndices['resultsTimestampIndex']:
                evaluationBagTime = evaluationIndices['resultsTimestampIndex'][imgTime]
            else:
                evaluationBagTime = None
                outputFile.write('Warning: No evaluation results found for image timestamp %.6f\n' % imgTime.to_sec())
                
            # Check for labels at this timestamp
            if imgTime in labelTimes:
                labelBagTime = labelTimes[imgTime]
            else:
                labelBagTime = None
                outputFile.write('Warning: No labels found for image timestamp %.6f\n' % imgTime.to_sec())
            
            if evaluationBagTime == None or labelBagTime == None:
                continue
            
            # Get ground truth image and convert to labels
            groundTruth = None
            for topic, msg, t in labelBags[labelBagTime[0]].read_messages(start_time = labelBagTime[1], end_time = labelBagTime[1]):
                if msg._type == sensor_msgs.msg.Image._type:
                    groundTruth = images.arrayFromImgMsg(msg)
            if groundTruth == None: raise RuntimeError('Unexpectedly couldn\'t read ground truth labels')
            groundTruthClasses = np.ndarray(groundTruth.shape[0:2], np.uint8)
            for y in xrange(groundTruth.shape[0]):
                for x in xrange(groundTruth.shape[1]):
                    groundTruthColor = groundTruth[y, x, :].reshape([3])
                    if (groundTruthColor == np.array([255, 0, 0])).all():
                        groundTruthClasses[y, x] = 0
                    elif (groundTruthColor == np.array([0, 255, 0])).all():
                        groundTruthClasses[y, x] = 1
                    elif (groundTruthColor == np.array([255, 0, 255])).all():
                        groundTruthClasses[y, x] = 2
                    elif (groundTruthColor == np.array([0, 0, 255])).all():
                        groundTruthClasses[y, x] = 3
                    elif (groundTruthColor == np.array([0, 0, 0])).all():
                        groundTruthClasses[y, x] = 255
                    else:
                        raise RuntimeError('Unexpected color in ground truth labels')
            
            # Get results image
            results = None
            for topic, msg, t in evaluationBag.read_messages(start_time = evaluationBagTime, end_time = evaluationBagTime):
                if msg._type == generalflow_rosmsgs.msg.FullImageErrorVisualization._type:
                    results = images.arrayFromImgMsg(msg.indicators)
                    curFrame = images.arrayFromImgMsg(msg.curFrame)
                    imgHeight = msg.curFrame.height
                    print 'Iteration %d' % msg.iteration
            if results == None: raise RuntimeError('Unexpectedly couldn\'t read results')
            resultsClasses = np.ndarray(curFrame.shape[0:2], np.uint8)
            if imgHeight * 4 != results.shape[0]:
                raise RuntimeError('Incorrect number of labels')
            for y in xrange(imgHeight):
                for x in xrange(results.shape[1]):
                    indicators = results[(4*y):(4*y+4), x].reshape([4])
                    maxClass = indicators.argmax()
                    resultsClasses[y, x] = maxClass
            
            # Scale and crop ground truth image
            scaleFactor = float(resultsClasses.shape[1]) / float(groundTruthClasses.shape[1])
            groundTruthClasses = cv2.resize(groundTruthClasses, (0, 0), fx = scaleFactor, fy = scaleFactor)
            groundTruthClasses = groundTruthClasses[0:resultsClasses.shape[0], :]
            
            # Do analysis
            resultsNegatives = np.logical_or(resultsClasses == np.array(1), resultsClasses == np.array(3))
            resultsPositivesWoutUnknown = resultsClasses == np.array(2)
            resultsPositivesWithUnknown = np.logical_or(resultsClasses == np.array(0), resultsClasses == np.array(2))
            truthNegatives = np.logical_or(groundTruthClasses == np.array(1), groundTruthClasses == np.array(3))
            truthPositives = (groundTruthClasses == np.array(2))
            
            # False negatives
            falseNegatives = np.logical_and(resultsNegatives, truthPositives)
            cv2.imwrite('%s/falseNegative_%.6f.png' % (outputDirectory, imgTime.to_sec()), images.shadeImage(curFrame, falseNegatives))
            
            # False positives
            falsePositivesWoutUnknown = np.logical_and(resultsPositivesWoutUnknown, truthNegatives)
            falsePositivesWithUnknown = np.logical_and(resultsPositivesWithUnknown, truthNegatives)
            cv2.imwrite('%s/falsePositiveWoutUnknown_%.6f.png' % (outputDirectory, imgTime.to_sec()), images.shadeImage(curFrame, falsePositivesWoutUnknown))
            cv2.imwrite('%s/falsePositiveWithUnknown_%.6f.png' % (outputDirectory, imgTime.to_sec()), images.shadeImage(curFrame, falsePositivesWithUnknown))
            
            # True positives
            truePositivesWoutUnknown = np.logical_and(resultsPositivesWoutUnknown, truthPositives)
            truePositivesWithUnknown = np.logical_and(resultsPositivesWithUnknown, truthPositives)
            cv2.imwrite('%s/truePositiveWoutUnknown_%.6f.png' % (outputDirectory, imgTime.to_sec()), images.shadeImage(curFrame, truePositivesWoutUnknown))
            cv2.imwrite('%s/truePositiveWithUnknown_%.6f.png' % (outputDirectory, imgTime.to_sec()), images.shadeImage(curFrame, truePositivesWithUnknown))
            
            # True negatives
            trueNegatives = np.logical_and(resultsNegatives, truthNegatives)
            cv2.imwrite('%s/trueNegative_%.6f.png' % (outputDirectory, imgTime.to_sec()), images.shadeImage(curFrame, trueNegatives))
            
            # Rates
            totalTruthPositives = totalTruthPositives + np.count_nonzero(truthPositives)
            totalTruthNegatives = totalTruthNegatives + np.count_nonzero(truthNegatives)
            outputFile.write('%.6f:\n' % imgTime.to_sec())
            totalFalseNegatives = totalFalseNegatives + np.count_nonzero(falseNegatives)
            if np.count_nonzero(truthPositives) > 0:
                falseNegRate = float(np.count_nonzero(falseNegatives)) / float(np.count_nonzero(truthPositives))
                outputFile.write('  False neg / actual pos:               %.4f\n' % (falseNegRate))
            else:
                outputFile.write('  False neg / actual pos:               Undef\n')
            falsePosWoutRate = float(np.count_nonzero(falsePositivesWoutUnknown)) / float(np.count_nonzero(truthNegatives))
            totalFalsePositivesWout = totalFalsePositivesWout + np.count_nonzero(falsePositivesWoutUnknown)
            outputFile.write('  False pos w/out unknown / actual neg: %.4f\n' % (falsePosWoutRate))
            falsePosWithRate = float(np.count_nonzero(falsePositivesWithUnknown)) / float(np.count_nonzero(truthNegatives))
            totalFalsePositivesWith = totalFalsePositivesWith + np.count_nonzero(falsePositivesWithUnknown)
            outputFile.write('  False pos w/ unknown / actual neg:    %.4f\n' % (falsePosWithRate))
            trueNegRate = float(np.count_nonzero(trueNegatives)) / float(np.count_nonzero(truthNegatives))
            totalTrueNegatives = totalTrueNegatives + np.count_nonzero(trueNegatives)
            outputFile.write('  True neg / actual neg:                %.4f\n' % (trueNegRate))
            totalTruePositivesWout = totalTruePositivesWout + np.count_nonzero(truePositivesWoutUnknown)
            totalTruePositivesWith = totalTruePositivesWith + np.count_nonzero(truePositivesWithUnknown)
            if np.count_nonzero(truthPositives) > 0:
                truePosWoutRate = float(np.count_nonzero(truePositivesWoutUnknown)) / float(np.count_nonzero(truthPositives))
                truePosWithRate = float(np.count_nonzero(truePositivesWithUnknown)) / float(np.count_nonzero(truthPositives))
                outputFile.write('  True pos w/out unknown / actual pos:  %.4f\n' % (truePosWoutRate))
                outputFile.write('  True pos w/ unknown / actual pos:     %.4f\n' % (truePosWithRate))
            else:
                outputFile.write('  True pos w/out unknown / actual pos:  Undef\n')
                outputFile.write('  True pos w/ unknown / actual pos:     Undef\n')
            outputFile.write('  Ground truth positives:               %.4f\n' % (float(np.count_nonzero(truthPositives)) / float(resultsClasses.size)))
        
        # Write totals
        outputFile.write('\n')
        outputFile.write('Total false neg / actual pos:       %.4f\n' % (float(totalFalseNegatives) / float(totalTruthPositives)))
        outputFile.write('Total false pos w/out / actual neg: %.4f\n' % (float(totalFalsePositivesWout) / float(totalTruthNegatives)))
        outputFile.write('Total false pos with / actual neg:  %.4f\n' % (float(totalFalsePositivesWith) / float(totalTruthNegatives)))
        outputFile.write('Total true neg / actual neg:        %.4f\n' % (float(totalTrueNegatives) / float(totalTruthNegatives)))
        outputFile.write('Total true pos w/out / actual pos:  %.4f\n' % (float(totalTruePositivesWout) / float(totalTruthPositives)))
        outputFile.write('Total true pos with / actual pos:   %.4f\n' % (float(totalTruePositivesWith) / float(totalTruthPositives)))
        
        outputFile.close()
                
    @PySide.QtCore.Slot()
    def on_actionImport_labeled_images_triggered(self):
        directory = q.QFileDialog.getExistingDirectory(self, 'Choose directory of labeled images to import')
        fileToWrite = q.QFileDialog.getSaveFileName(self, 'Choose bag file to create containing labeled images')
    
    @PySide.QtCore.Slot()
    def on_timeSlider_valueChanged(self):
        self.loadData()
        
    @PySide.QtCore.Slot()
    def on_segmentStartButton_clicked(self):
        self.segmentStart = self.timeSlider.value()
        self.updateLabels()
        
    @PySide.QtCore.Slot()
    def on_segmentEndButton_clicked(self):
        self.segmentEnd = self.timeSlider.value()
        self.updateLabels()
        
    @PySide.QtCore.Slot()
    def on_jumpStartButton_clicked(self):
        self.timeSlider.setValue(self.segmentStart)
        
    @PySide.QtCore.Slot()
    def on_jumpEndButton_clicked(self):
        self.timeSlider.setValue(self.segmentEnd)
    
    def updateControlLimits(self):
        self.timeSlider.setRange(0, len(self.indices['frameIndex']) - 1)
        self.segmentStart = 0
        self.segmentEnd = len(self.indices['frameIndex']) - 1
        
    def loadData(self):
        self.updateLabels()        
        msgs = robotLogs.getMessages(self.logBag, self.indices, self.timeSlider.value())
        haveImageUpdate = False
        for msg in msgs:
            if msg._type == sensor_msgs.msg.Image._type:
                if haveImageUpdate:
                    self.warningLabel.setText('<span style=" color:#ff0000;">Warning: multiple images with this timestamp</span>')
                else:
                    self.currentImage.setPixmap(images.pixmapFromArray(images.arrayFromImgMsg(msg)))
                    haveImageUpdate = True
                    self.warningLabel.setText('')
                
    def updateLabels(self):
        self.currentTimeLabel.setText('%.6f' % self.indices['frameIndex'][self.timeSlider.value()].msgTime.to_sec())
        self.segmentStartLabel.setText('%.6f' % self.indices['frameIndex'][self.segmentStart].msgTime.to_sec())
        self.segmentEndLabel.setText('%.6f' % self.indices['frameIndex'][self.segmentEnd].msgTime.to_sec())
       
if __name__ == '__main__':
    app = q.QApplication(sys.argv)
    frame = Labeler()
    frame.show()
    app.exec_()
