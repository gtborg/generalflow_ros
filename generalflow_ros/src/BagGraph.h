/**
 * @file    BagGraph.h
 * @brief   
 * @author  Richard Roberts
 * @created Oct 5, 2013
 */

#pragma once

#include <gtsam/linear/VectorValues.h>
#include <gtsam/linear/Errors.h>
#include <gtsam/linear/GaussianFactorGraph.h>
#include <gtsam/nonlinear/Values.h>

#include <rosbag/bag.h>
#include <tbb/tbb.h>

#include <generalflow_flow/flowOps.h>
#include "dllexport.h"
#include "learningUtils.h"

namespace generalflow_ros
{
  /// A class that behaves like a GaussianFactorGraph, but performs calculations out-of core by
  /// iterating over a bag file.
  class generalflow_ros_EXPORT BagGraph
  {
  protected:
    ImageInfo imageInfo_;
    int nthFrame_;
    double imagePreScale_;
    ImageROI roi_;
    bool merge_;

  protected:
    std::vector<boost::shared_ptr<const rosbag::Bag> > bags_;

    BagGraph(const boost::shared_ptr<const rosbag::Bag>& bag,
        int nthFrame, double imagePreScale, const ImageROI& roi, bool merge) :
      bags_(1, bag), imageInfo_(getImageInfo(bag, imagePreScale, roi)),
      nthFrame_(nthFrame), imagePreScale_(imagePreScale), roi_(roi), merge_(merge) {}

    BagGraph(const std::vector<boost::shared_ptr<const rosbag::Bag> >& bags,
        int nthFrame, double imagePreScale, const ImageROI& roi, bool merge) :
      bags_(bags), imageInfo_(getImageInfo(bags.front(), imagePreScale, roi)),
      nthFrame_(nthFrame), imagePreScale_(imagePreScale), roi_(roi), merge_(merge) {}

    virtual ~BagGraph() {}

    /// Struct to store info for a whole frame that can be shared across pixel blocks.  This can
    /// be extended in derived classes to store additional information.
    struct FrameInfo
    {
      size_t frameNum; ///< The current frame number

      /// Constructor
      FrameInfo(size_t frameNum) : frameNum(frameNum) {}

      /// Virtual destructor
      virtual ~FrameInfo() {}
    };

    /// Override in derived class - create (optionally) a class derived from FrameInfo.
    virtual boost::shared_ptr<FrameInfo> createFrameInfo(size_t framenum, size_t bagnum, const ros::Time& stamp,
        const generalflow::flow::ImagePairGrad::shared_ptr& imgPair) const;

    /// Override in derived class - creates factors for every pixel in a block of pixels.  Will
    /// be called concurrently.
    virtual void createPixelFactors(const boost::shared_ptr<FrameInfo>& frameInfo,
        const tbb::blocked_range<size_t>& pixelIndexRange,
        const boost::shared_ptr<generalflow::flow::ImagePairGrad>& imgPair,
        gtsam::GaussianFactorGraph::iterator firstFactorPtr,
        gtsam::GaussianFactorGraph::iterator lastFactorPtr) const = 0;

    /// Override in derived class - create "static" factors that do not depend on the frames.
    virtual gtsam::GaussianFactorGraph createStaticFactors() const;

    template<typename F>
    void foreachFrame(const F& f) const;

    void foreachFrameChunk(const boost::shared_ptr<FrameInfo>& frameInfo,
        const tbb::blocked_range<size_t>& r,
        const boost::shared_ptr<generalflow::flow::ImagePairGrad>& imgPair,
        gtsam::GaussianFactorGraph& graph) const;

    template<typename F>
    void foreachPixel(const F& f) const;

    template<typename F>
    void foreachPixelChunk(const boost::shared_ptr<FrameInfo>& frameInfo,
        const tbb::blocked_range<size_t>& r,
        const boost::shared_ptr<generalflow::flow::ImagePairGrad>& imgPair, const F& f) const;

  public:
    /// Accumulate all factors - only for debugging, this may allocate too much memory on large
    /// bag files.
    gtsam::GaussianFactorGraph getAllFactors() const;

    /// Compute the gradient
    gtsam::VectorValues gradient(const gtsam::VectorValues& x0) const;

    /// Multiply with a solution vector
    gtsam::Errors operator*(const gtsam::VectorValues& x) const;

    ///** In-place version e <- A*x that overwrites e. */
    void multiplyInPlace(const gtsam::VectorValues& x, gtsam::Errors& e) const;

    /** x += alpha*A'*e */
    void transposeMultiplyAdd(double alpha, const gtsam::Errors& e, gtsam::VectorValues& x) const;

    /// Conjugate gradient descent
    gtsam::VectorValues optimizeConjugateGradients(
        const gtsam::VectorValues& start, double threshold, double maxIter) const;

    const std::vector<boost::shared_ptr<const rosbag::Bag> >& bags() const { return bags_; }

    const ImageInfo imageInfo() const { return imageInfo_; }


  };
}

