/**
 * @file    Basis2Graph.cpp
 * @brief   
 * @author  Richard Roberts
 * @created Oct 6, 2013
 */

#include "Basis2Graph.h"

#include <generalflow_denselabeling/BasisNFactor.h>
#include <generalflow_denselabeling/BasisNTransFactor.h>
#include <generalflow_denselabeling/VelocityNFactor.h>
#include <boost/make_shared.hpp>
#include <gtsam/nonlinear/Symbol.h>
#include <gtsam/base/LieMatrix.h>
#include <gtsam/slam/BetweenFactor.h>
#include <gtsam/slam/PriorFactor.h>
#include <gtsam/linear/GaussianBayesTree.h>
#include <fstream>

using namespace gtsam;
using namespace std;
using namespace generalflow::flow;
using namespace generalflow::denselabeling;

ofstream globalParamsFile;

namespace generalflow_ros
{
  _WriteThread visualizationWriter;

  /* ************************************************************************* */
  boost::shared_ptr<BagGraph::FrameInfo> Basis2Graph::createFrameInfo(size_t framenum,
      const generalflow::flow::ImagePairGrad::shared_ptr& imgPair) const
  {
    const DenseIndex wid = imgPair->prevFrame().cols();
    const DenseIndex hei = imgPair->prevFrame().rows();
    const DenseIndex q = basisLinearizationPoint_->at<LieMatrix>(symbol('a', 0)).cols();
    const DenseIndex r = basisLinearizationPoint_->at<LieMatrix>(symbol('b', 0)).cols();

    boost::shared_ptr<FrameInfo> frameInfo = boost::make_shared<FrameInfo>(
        framenum, visualize_ && iteration_ % 5 == 1, visualizationPublisher_, iteration_, imageInfo());

    // Get basis
    Matrix basis1(2*wid*hei, q + r), basis2(2*wid*hei, r);
    vector<Matrix> basisTrans(1, Matrix(2*wid*hei, q));
    for(size_t pix = 0; pix < wid*hei; ++pix)
    {
      basisTrans[0].middleRows(2*pix, 2) = basisLinearizationPoint_->at<LieMatrix>(symbol('a', pix));
      basis2.middleRows(2*pix, 2) = basisLinearizationPoint_->at<LieMatrix>(symbol('b', pix));
      basis1.middleRows(2*pix, 2).leftCols(r) = basis2.middleRows(2*pix, 2); // Set to rotation basis
      basis1.middleRows(2*pix, 2).rightCols(q) = basisTrans[0].middleRows(2*pix, 2); // Set to translation basis
    }

    // Solve indicators
    //cout << "Solving indicators frame " << framenum << endl;

    const Matrix sigmaV = velocities_->at<LieMatrix>(symbol('S', framenum));
    frameInfo->indicators = solveIndicators(*imgPair, basis1, basis2, velocities_->at<LieVector>(framenum),
        *classPriors_,
        sigmas_.flowSigmaF, sigmas_.flowSigmaB1, sigmas_.flowSigmaB2,
        sigmas_.pixSigmaF, sigmas_.pixSigmaB1, sigmas_.pixSigmaB2,
        sigmaV,
        frameInfo->visualization);

    // Solve velocity
    //cout << "Solving velocity frame " << framenum << endl;
    GaussianFactorGraph velocityGraph;
    velocityGraph += VelocityNFactor(framenum, basis2, basisTrans, sigmas_, frameInfo->indicators, imgPair).linearize(*velocities_);
    //velocityGraph += Velocity2Factor(framenum, basis1, basis2, sigmas_, classPriors_, imgPair).linearize(*velocities_);
    SharedDiagonal priorModel = noiseModel::Isotropic::Sigma(q+r, 1e4);
    velocityGraph += PriorFactor<LieVector>(framenum, LieVector(Vector::Zero(q+r)), priorModel).linearize(*velocities_);
    GaussianBayesTree::shared_ptr R = velocityGraph.eliminateMultifrontal(boost::none, EliminateQR);
    VectorValues update = R->optimize();
    LieVector newVelocity = velocities_->at<LieVector>(framenum).retract(update.at(framenum));
    LieMatrix covV = R->marginalCovariance(framenum);
    velocities_->update(framenum, newVelocity);
    velocities_->update(symbol('S', framenum), covV);
    frameInfo->velocity = newVelocity;

    return frameInfo;
  }

  /* ************************************************************************* */
  GaussianFactorGraph Basis2Graph::createStaticFactors() const
  {
    const DenseIndex wid = imageInfo().width;
    const DenseIndex hei = imageInfo().height;
    const DenseIndex q1 = basisLinearizationPoint_->at<LieMatrix>(symbol('a', 0)).cols();
    const DenseIndex q2 = basisLinearizationPoint_->at<LieMatrix>(symbol('b', 0)).cols();
    NonlinearFactorGraph graph;
    graph.reserve(wid*hei*4);
    const double betweenSigma = 1.0;
    const double betweenPrec = 1.0 / (betweenSigma * betweenSigma);
    SharedDiagonal noise1 = noiseModel::Isotropic::Sigma(2*q1, betweenSigma);
    SharedDiagonal noise2 = noiseModel::Isotropic::Sigma(2*q2, betweenSigma);
    //SharedDiagonal noiseS = noiseModel::Isotropic::Sigma(2*q2, 1e-4);
    //SharedDiagonal noiseS = noiseModel::Constrained::All(2*q2, 1e6);
    SharedDiagonal noiseP1 = noiseModel::Isotropic::Sigma(2*q1, 1e4);
    SharedDiagonal noiseP2 = noiseModel::Isotropic::Sigma(2*q2, 1e4);
    static bool wroteParams = false;
    if(!wroteParams)
    {
      globalParamsFile << "betweenSigma = " << betweenSigma << endl;
      //globalParamsFile << "noiseSSigma = " << noiseS->sigma(0) << endl;
      //globalParamsFile << "noisePSigma = " << noiseP1->sigma(0) << endl;
      wroteParams = true;
    }
    for(DenseIndex j = 0; j < wid; ++j) {
      for(DenseIndex i = 0; i < hei; ++i) {
        size_t pix = j*hei + i;
        size_t pixR = pix + hei;
        size_t pixD = pix + 1;
        if(j != wid - 1)
        {
          graph += BetweenFactor<LieMatrix>(symbol('a', pix), symbol('a', pixR),
              LieMatrix(Matrix::Zero(2, q1)), noise1);
          if(updateRotationBasis_)
            graph += BetweenFactor<LieMatrix>(symbol('b', pix), symbol('b', pixR),
                LieMatrix(Matrix::Zero(2, q2)), noise2);
        }
        if(i != hei - 1)
        {
          graph += BetweenFactor<LieMatrix>(symbol('a', pix), symbol('a', pixD),
              LieMatrix(Matrix::Zero(2, q1)), noise1);
          if(updateRotationBasis_)
            graph += BetweenFactor<LieMatrix>(symbol('b', pix), symbol('b', pixD),
                LieMatrix(Matrix::Zero(2, q2)), noise2);
        }
        //graph += SameRotationBasisFactor(symbol('a', pix), symbol('b', pix), noiseS);
        //graph += PriorFactor<LieMatrix>(symbol('a', pix), LieMatrix(Matrix::Zero(2, q1)), noiseP1);
        //graph += PriorFactor<LieMatrix>(symbol('b', pix), LieMatrix(Matrix::Zero(2, q2)), noiseP2);
      }
    }
    return *graph.linearize(*basisLinearizationPoint_);
  }

  /* ************************************************************************* */
  void Basis2Graph::createPixelFactors(const boost::shared_ptr<Base::FrameInfo>& baseFrameInfo,
      const tbb::blocked_range<size_t>& pixelIndexRange,
      const boost::shared_ptr<generalflow::flow::ImagePairGrad>& imgPair,
      gtsam::GaussianFactorGraph::iterator firstFactorPtr,
      gtsam::GaussianFactorGraph::iterator lastFactorPtr) const
  {
    const FrameInfo& frameInfo = static_cast<const FrameInfo&>(*baseFrameInfo);
    for(size_t pix = pixelIndexRange.begin(); pix != pixelIndexRange.end(); ++pix)
    {
      DenseIndex i = pix % imgPair->curFrame().rows();
      DenseIndex j = pix / imgPair->curFrame().rows();
      //cout << frameInfo.indicators.col(pix).transpose() << endl;
      if(updateRotationBasis_)
        *(firstFactorPtr ++) = BasisNFactor(symbol('b', pix), gtsam::ListOfOne<Key>(symbol('a', pix)),
            frameInfo.velocity, sigmas_, frameInfo.indicators.col(pix), imgPair, j, i, frameInfo.visualization)
            .linearize(*basisLinearizationPoint_);
      else
        *(firstFactorPtr ++) = BasisNTransFactor(symbol('b', pix), gtsam::ListOfOne<Key>(symbol('a', pix)),
            frameInfo.velocity, sigmas_, frameInfo.indicators.col(pix), imgPair, j, i, frameInfo.visualization)
            .linearize(*basisLinearizationPoint_);

    }
  }
}




