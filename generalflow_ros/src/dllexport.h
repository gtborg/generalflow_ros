/**
 * @file     dllexport.h
 * @brief    Symbols for exporting classes and methods from DLLs
 * @author   Richard Roberts
 * @date     Mar 9, 2013
 */

// Macros for exporting DLL symbols on Windows
// Usage example:
// In header file:
//   class generalflow_ros_EXPORT MyClass { ... };
//   
// Results in the following declarations:
// When included while compiling the GTSAM library itself:
//   class __declspec(dllexport) MyClass { ... };
// When included while compiling other code against GTSAM:
//   class __declspec(dllimport) MyClass { ... };
#ifdef _WIN32
#  ifdef generalflow_ros_EXPORTS
#    define generalflow_ros_EXPORT __declspec(dllexport)
#    define generalflow_ros_EXTERN_EXPORT __declspec(dllexport) extern
#  else
#    ifndef generalflow_ros_IMPORT_STATIC
#      define generalflow_ros_EXPORT __declspec(dllimport)
#      define generalflow_ros_EXTERN_EXPORT __declspec(dllimport)
#    else /* generalflow_ros_IMPORT_STATIC */
#      define generalflow_ros_EXPORT
#      define generalflow_ros_EXTERN_EXPORT extern
#    endif /* generalflow_ros_IMPORT_STATIC */
#  endif /* generalflow_ros_EXPORTS */
#else /* _WIN32 */
#  define generalflow_ros_EXPORT
#  define generalflow_ros_EXTERN_EXPORT extern
#endif

