/**
 * @file    Basis2Graph.h
 * @brief   
 * @author  Richard Roberts
 * @created Oct 6, 2013
 */

#pragma once

#include "BagGraph.h"
#include <generalflow_ros/FullImageErrorVisualization.h>

#include <map>
#include <boost/thread.hpp>
#include <boost/make_shared.hpp>
#include <tbb/tbb.h>

#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>

#include <generalflow_denselabeling/types.h>
#include <generalflow_flow/flowOps.h>

namespace generalflow_ros
{

  /* ************************************************************************* */
  class _WriteThread
  {
    bool shutdown;
    tbb::concurrent_bounded_queue<boost::shared_ptr<generalflow::flow::FullImageErrorVisualization> > visualizationQueue;
    boost::thread writerThread;

  public:
    _WriteThread() :
      shutdown(false), writerThread(boost::ref(*this))
    {
      visualizationQueue.set_capacity(5000);
    }

    void write(boost::shared_ptr<generalflow::flow::FullImageErrorVisualization> visualization)
    {
      visualizationQueue.push(visualization);
    }

    void operator()()
    {
      size_t count = 0;
      while(true)
      {
        while(true)
        {
          boost::shared_ptr<generalflow::flow::FullImageErrorVisualization> current;
          if(visualizationQueue.try_pop(current))
          {
            if(count % 100 == 0)
              std::cout << "Writing visualization frame " << current->framenum << " iteration " <<
              current->iteration << " (queue length " << visualizationQueue.size() << ")" << std::endl;
            current->write();
            ++ count;
          }
          else
          {
            if(shutdown)
              return;
            break; // If queue was empty, break the inner loop so we sleep for 100 ms
          }
        }
        boost::this_thread::sleep(boost::posix_time::milliseconds(100));
      }
    }

    ~_WriteThread()
    {
      shutdown = true;
      writerThread.join();
    }

  private:
    _WriteThread(const _WriteThread&) {}
  };

  generalflow_ros_EXTERN_EXPORT _WriteThread visualizationWriter;

  /* ************************************************************************* */
  class generalflow_ros_EXPORT Basis2Graph : public BagGraph
  {
  protected:
    bool updateRotationBasis_;
    boost::shared_ptr<gtsam::Values> velocities_;
    boost::shared_ptr<const gtsam::Values> basisLinearizationPoint_;
    generalflow::denselabeling::Sigmas sigmas_;
    boost::shared_ptr<const generalflow::denselabeling::ClassPriors> classPriors_;

    bool visualize_;
    boost::optional<ros::Publisher> visualizationPublisher_;
    int iteration_;

  public:

    typedef BagGraph Base;

    Basis2Graph(const std::vector<boost::shared_ptr<const rosbag::Bag> >& bags, int nthFrame, double imagePreScale, bool merge,
      bool updateRotationBasis,
      const boost::shared_ptr<gtsam::Values>& velocities,
      const boost::shared_ptr<const gtsam::Values>& basisLinearizationPoint,
      const generalflow::denselabeling::Sigmas sigmas,
      const boost::shared_ptr<const generalflow::denselabeling::ClassPriors>& classPriors,
      bool visualize = false, const boost::optional<ros::NodeHandle&>& visualizationNode = boost::none, int iteration = -1) :
      Base(bags, nthFrame, imagePreScale, merge), updateRotationBasis_(updateRotationBasis),
      velocities_(velocities), basisLinearizationPoint_(basisLinearizationPoint),
      sigmas_(sigmas), classPriors_(classPriors), visualize_(visualize), iteration_(iteration)
    {
      if(visualizationNode)
        visualizationPublisher_.reset(visualizationNode->advertise<FullImageErrorVisualization>("Basis2Graph/visualization", 1000));
    }

    struct FrameInfo : Base::FrameInfo
    {
      gtsam::LieVector velocity;
      gtsam::Matrix indicators;
      boost::shared_ptr<generalflow::flow::FullImageErrorVisualization> visualization;
      const boost::optional<ros::Publisher>& visualizationPublisher;

      FrameInfo(size_t framenum,
        bool visualize, const boost::optional<ros::Publisher>& visualizationPublisher,
        int iteration, const ImageInfo& imageInfo) :
        Base::FrameInfo(framenum), visualizationPublisher(visualizationPublisher)
      {
        if(visualize)
          visualization = boost::make_shared<generalflow::flow::FullImageErrorVisualization>(
              iteration, framenum, imageInfo.width, imageInfo.height, 3);
      }

      virtual ~FrameInfo()
      {
        if(visualization)
        {
          if(visualizationPublisher)
          {
            FullImageErrorVisualizationPtr visMsg = boost::make_shared<FullImageErrorVisualization>();
            visMsg->iteration = visualization->iteration;
            visMsg->framenum = visualization->framenum;

            visMsg->prevFrame = *cv_bridge::CvImage(visMsg->header,
              sensor_msgs::image_encodings::TYPE_32FC1, visualization->prevFrame).toImageMsg();
            visMsg->curFrame = *cv_bridge::CvImage(visMsg->header,
              sensor_msgs::image_encodings::TYPE_32FC1, visualization->curFrame).toImageMsg();
            visMsg->temporalDeriv = *cv_bridge::CvImage(visMsg->header,
              sensor_msgs::image_encodings::TYPE_32FC1, visualization->temporalDeriv).toImageMsg();
            visMsg->errs = *cv_bridge::CvImage(visMsg->header,
              sensor_msgs::image_encodings::TYPE_32FC1, visualization->errs).toImageMsg();
            visMsg->indicators = *cv_bridge::CvImage(visMsg->header,
              sensor_msgs::image_encodings::TYPE_32FC1, visualization->indicators).toImageMsg();
            visMsg->expectedFlow = *cv_bridge::CvImage(visMsg->header,
              sensor_msgs::image_encodings::TYPE_32FC1, visualization->expectedFlow).toImageMsg();

            visualizationPublisher->publish(visMsg);
          }
          else
          {
            visualizationWriter.write(visualization);
          }
        }
      }
    };

    virtual boost::shared_ptr<Base::FrameInfo> createFrameInfo(size_t framenum,
        const generalflow::flow::ImagePairGrad::shared_ptr& imgPair) const;

    virtual void createPixelFactors(const boost::shared_ptr<Base::FrameInfo>& frameInfo,
        const tbb::blocked_range<size_t>& pixelIndexRange,
        const boost::shared_ptr<generalflow::flow::ImagePairGrad>& imgPair,
        gtsam::GaussianFactorGraph::iterator firstFactorPtr,
        gtsam::GaussianFactorGraph::iterator lastFactorPtr) const;

    virtual gtsam::GaussianFactorGraph createStaticFactors() const;

    const boost::shared_ptr<gtsam::Values>& velocities() const { return velocities_; }
    const boost::shared_ptr<const gtsam::Values>& basisLinearizationPoint() const { return basisLinearizationPoint_; }
    const generalflow::denselabeling::Sigmas& sigmas() const { return sigmas_; }
    const boost::shared_ptr<const generalflow::denselabeling::ClassPriors>& classPriors() const { return classPriors_; }
  };

}

