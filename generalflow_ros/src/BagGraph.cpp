/**
 * @file    BagGraph.cpp
 * @brief   
 * @author  Richard Roberts
 * @created Oct 5, 2013
 */

#include "BagGraph.h"

#include <rosbag/view.h>
#include <sensor_msgs/Image.h>

#include <boost/foreach.hpp>
#include <boost/make_shared.hpp>
#include <boost/assign/list_of.hpp>

using namespace gtsam;
using namespace std;
using namespace generalflow::flow;
using boost::assign::list_of;

typedef tbb::spin_mutex AccumulatorMutex;

namespace generalflow_ros
{
  /* ************************************************************************* */
  boost::shared_ptr<BagGraph::FrameInfo> BagGraph::createFrameInfo(size_t framenum, size_t bagnum, const ros::Time& stamp,
      const generalflow::flow::ImagePairGrad::shared_ptr& imgPair) const
  {
    return boost::make_shared<BagGraph::FrameInfo> (framenum);
  }

  /* ************************************************************************* */
  static void _mergeGraphs(const tbb::blocked_range<size_t>& r,
      GaussianFactorGraph& mergeGraph, const GaussianFactorGraph& other)
  {
    for(size_t i = r.begin(); i != r.end(); ++i)
    {
      if(mergeGraph[i]->keys() != other[i]->keys())
        throw runtime_error("Inconsistent factors in merge mode");
      mergeGraph[i] = boost::make_shared<HessianFactor>(
          GaussianFactorGraph(list_of(mergeGraph[i])(other[i])));
    }
  }

  /* ************************************************************************* */
  template<typename F>
  void BagGraph::foreachFrame(const F& f) const
  {
    // Set up view to loop over images
    vector<string> topics;
    topics.push_back("/camera/image_raw");
    topics.push_back("camera/image_raw");
    //topics.push_back("/vehicle_speed");
    //topics.push_back("/servo_status");

    GaussianFactorGraph mergedGraph;
    size_t framenum = 1;
    size_t bagnum = 0;

    BOOST_FOREACH(const boost::shared_ptr<const rosbag::Bag>& bag, bags_)
    {
      rosbag::View view(*bag, rosbag::TopicQuery(topics));

      // Loop over image messages and create factors, and call callback f
      boost::shared_ptr<ImagePairGrad> imgPair;
      BOOST_FOREACH(rosbag::MessageInstance const m, view)
      {
        if(sensor_msgs::ImageConstPtr img = m.instantiate<sensor_msgs::Image>())
        {
          // Scale the image using OpenCV to be sure we get the correct rounding
          cv::Mat imgIn(img->height, img->width, CV_8UC1, const_cast<uint8_t*>(&img->data[0]));
          if(roi_.x >= 0)
            imgIn(cv::Rect(roi_.x, roi_.y, roi_.w, roi_.h)).copyTo(imgIn);
          cv::Mat imgOut;
          cv::resize(imgIn, imgOut, cv::Size(), imagePreScale_, imagePreScale_, CV_INTER_AREA);

          const Eigen::Map<const Eigen::Matrix<uint8_t, -1, -1, Eigen::RowMajor> > imgM(
              imgOut.data, imgOut.rows, imgOut.cols);
          if(!imgPair)
            imgPair = boost::make_shared<ImagePairGrad>(imgM.cast<double>() / 255.0, imgM.cast<double>() / 255.0);
          else
            imgPair = boost::make_shared<ImagePairGrad>(*imgPair, imgM.cast<double>() / 255.0);

          if(framenum % nthFrame_ == 0)
          {
            // Build a graph of basis factors for just this frame
            GaussianFactorGraph graph;
            graph.resize(imgOut.cols * imgOut.rows);
            boost::shared_ptr<FrameInfo> frameInfo = createFrameInfo(framenum, bagnum, img->header.stamp, imgPair);
            tbb::parallel_for(tbb::blocked_range<size_t>(0, imgOut.cols * imgOut.rows),
                boost::bind(&BagGraph::foreachFrameChunk, this, boost::cref(frameInfo),
                    _1, boost::cref(imgPair), boost::ref(graph)));
            //          foreachFrameChunk(frameInfo, tbb::blocked_range<size_t>(0, imgOut.cols * imgOut.rows),
            //              imgPair, graph);

            if(merge_)
            {
              // Merge factors if in merge mode
              if(mergedGraph.empty())
              {
                BOOST_FOREACH(const GaussianFactor::shared_ptr& factor, graph)
                  {
                  if(HessianFactor::shared_ptr hessian = boost::dynamic_pointer_cast<HessianFactor>(factor))
                    mergedGraph += hessian;
                  else
                    mergedGraph += HessianFactor(*factor);
                  }
                //cout << "Merged:\n" << mergedGraph.augmentedHessian() << endl;
              }
              else
              {
                if(mergedGraph.size() != graph.size())
                  throw runtime_error("Different graph sizes in merge mode");
                tbb::parallel_for(tbb::blocked_range<size_t>(0, mergedGraph.size()),
                    boost::bind(&_mergeGraphs, _1, boost::ref(mergedGraph), boost::cref(graph)));
                //cout << "To add:\n" << graph.augmentedHessian() << endl;
                //cout << "Merged:\n" << mergedGraph.augmentedHessian() << endl;
              }
            }
            else
            {
              // Call functor on frame graph if not in merge mode
              f(graph);
            }
          }

          if(framenum % 100 == 0)
            cout << "Frame " << framenum << endl;

          ++ framenum;
        }
      }

      ++ bagnum;
    }

    // Call functor on merged graph if in merge mode
    if(merge_)
      f(mergedGraph);

    // Now call callback for static factors
    f(createStaticFactors());
  }

  /* ************************************************************************* */
  void BagGraph::foreachFrameChunk(const boost::shared_ptr<FrameInfo>& frameInfo,
      const tbb::blocked_range<size_t>& r,
      const boost::shared_ptr<ImagePairGrad>& imgPair, GaussianFactorGraph& graph) const
  {
    createPixelFactors(frameInfo, r, imgPair, graph.begin() + r.begin(), graph.begin() + r.end());
  }

  /* ************************************************************************* */
  template<typename F>
  void BagGraph::foreachPixel(const F& f) const
  {
    // Set up view to loop over images
    vector<string> topics;
    topics.push_back("/camera/image_raw");
    topics.push_back("camera/image_raw");
    //topics.push_back("/vehicle_speed");
    //topics.push_back("/servo_status");

    size_t framenum = 1;
    size_t bagnum = 0;

    BOOST_FOREACH(const boost::shared_ptr<const rosbag::Bag>& bag, bags_)
    {
      rosbag::View view(*bag, rosbag::TopicQuery(topics));

      // Loop over image messages and call callback f for each factor
      boost::shared_ptr<ImagePairGrad> imgPair;
      BOOST_FOREACH(rosbag::MessageInstance const m, view)
      {
        if(sensor_msgs::ImageConstPtr img = m.instantiate<sensor_msgs::Image>())
        {
          if(framenum % nthFrame_ == 0)
          {
            // Scale the image using OpenCV to be sure we get the correct rounding
            cv::Mat imgIn(img->height, img->width, CV_8UC1, const_cast<uint8_t*>(&img->data[0]));
            if(roi_.x >= 0)
              imgIn(cv::Rect(roi_.x, roi_.y, roi_.w, roi_.h)).copyTo(imgIn);
            cv::Mat imgOut;
            cv::resize(imgIn, imgOut, cv::Size(), imagePreScale_, imagePreScale_, CV_INTER_AREA);

            const Eigen::Map<const Eigen::Matrix<uint8_t, -1, -1, Eigen::RowMajor> > imgM(imgOut.data, imgOut.rows, imgOut.cols);
            if(!imgPair)
              imgPair = boost::make_shared<ImagePairGrad>(imgM.cast<double>() / 255.0, imgM.cast<double>() / 255.0);
            else
              imgPair = boost::make_shared<ImagePairGrad>(*imgPair, imgM.cast<double>() / 255.0);

            // Build a graph of basis factors for just this frame
            GaussianFactorGraph graph;
            graph.resize(imgOut.cols * imgOut.rows);
            boost::shared_ptr<FrameInfo> frameInfo = createFrameInfo(framenum, bagnum, img->header.stamp, imgPair);
            tbb::parallel_for(tbb::blocked_range<size_t>(0, imgOut.cols*imgOut.rows),
                boost::bind(&BagGraph::foreachPixelChunk<F>, this, frameInfo, _1,
                    boost::cref(imgPair), boost::cref(f)));
            //          foreachPixelChunk<F>(frameInfo, tbb::blocked_range<size_t>(0, imgOut.cols*imgOut.rows),
            //              imgPair, f);
          }

          if(framenum % 100 == 0)
            cout << "Frame " << framenum << endl;

          ++ framenum;
        }
      }

      ++ bagnum;
    }

    // Call callback for static factors
    GaussianFactorGraph staticFactors = createStaticFactors();
    BOOST_FOREACH(const GaussianFactor::shared_ptr& factor, staticFactors) {
      f(*factor);
    }
  }

  /* ************************************************************************* */
  template<typename F>
  void BagGraph::foreachPixelChunk(const boost::shared_ptr<FrameInfo>& frameInfo,
      const tbb::blocked_range<size_t>& r,
      const boost::shared_ptr<generalflow::flow::ImagePairGrad>& imgPair, const F& f) const
  {
    GaussianFactorGraph graph;
    graph.resize(r.end() - r.begin());
    createPixelFactors(frameInfo, r, imgPair, graph.begin(), graph.end());
    BOOST_FOREACH(const GaussianFactorGraph::sharedFactor& factor, graph)
      f(*factor);
  }

  /* ************************************************************************* */
  GaussianFactorGraph BagGraph::createStaticFactors() const
  {
    return GaussianFactorGraph();
  }

  /* ************************************************************************* */
  namespace {
    struct GetAllFactorsAdder {
      GaussianFactorGraph& result;
      GetAllFactorsAdder(GaussianFactorGraph& result) : result(result) {}
      void operator()(const GaussianFactorGraph& factors) const {
        result += factors;
      }
    };
  }

  /* ************************************************************************* */
  GaussianFactorGraph BagGraph::getAllFactors() const
  {
    GaussianFactorGraph result;
    foreachFrame(GetAllFactorsAdder(result));
    return result;
  }

  /* ************************************************************************* */
  static void gradientAdder(VectorValues& gradient, const GaussianFactor& factor,
      const VectorValues& x0, AccumulatorMutex& mutex)
  {
    const JacobianFactor& jacobian = dynamic_cast<const JacobianFactor&>(factor);

    Vector e = jacobian.error_vector(x0);
    AccumulatorMutex::scoped_lock lock(mutex);
    jacobian.transposeMultiplyAdd(1.0, e, gradient);
  }

  /* ************************************************************************* */
  VectorValues BagGraph::gradient(const gtsam::VectorValues& x0) const
  {
    // Loop over messages and accumulate gradient
    cout << "Computing gradient..." << endl;
    VectorValues gradient;
    AccumulatorMutex mutex;

    foreachPixel(boost::bind(gradientAdder, boost::ref(gradient), _1, boost::cref(x0), boost::ref(mutex)));

    return gradient;
  }

  /* ************************************************************************* */
  static void productAdder(Errors& errors, const GaussianFactorGraph& graph, const VectorValues& x)
  {
    Errors oneGraphErrors = graph.gaussianErrors(x);
    errors.insert(errors.end(), oneGraphErrors.begin(), oneGraphErrors.end());
  }

  /* ************************************************************************* */
  Errors BagGraph::operator*(const VectorValues& x) const
  {
    // Loop over messages and accumulate errors
    cout << "Computing A*x..." << endl;
    Errors errors;

    foreachFrame(boost::bind(productAdder, boost::ref(errors), _1, boost::cref(x)));

    return errors;
  }

  /* ************************************************************************* */
  static void multiplyInPlaceAdder(Errors::iterator& e, const GaussianFactorGraph& graph, const VectorValues& x)
  {
    graph.multiplyInPlace(x, e);
    advance(e, graph.size());
  }

  /* ************************************************************************* */
  void BagGraph::multiplyInPlace(const VectorValues& x, Errors& e) const
  {
    cout << "Computing A*x (in-place version)..." << endl;

    Errors::iterator eIt = e.begin();
    foreachFrame(boost::bind(multiplyInPlaceAdder, boost::ref(eIt), _1, boost::cref(x)));
  }

  /* ************************************************************************* */
  static void transposeMultiplyAddAdder(Errors::const_iterator& eIt, const GaussianFactorGraph& graph, double alpha, VectorValues& x)
  {
    BOOST_FOREACH(const GaussianFactor::shared_ptr& Ai_G, graph) {
      JacobianFactor::shared_ptr Ai = boost::dynamic_pointer_cast<JacobianFactor>(Ai_G);
      Ai->transposeMultiplyAdd(alpha, *(eIt++), x);
    }
  }

  /* ************************************************************************* */
  // x += alpha*A'*e
  void BagGraph::transposeMultiplyAdd(double alpha, const Errors& e, VectorValues& x) const
  {
    cout << "Computing transposeMultiplyAdd..." << endl;
    // For each factor add the gradient contribution
    Errors::const_iterator ei = e.begin();
    foreachFrame(boost::bind(transposeMultiplyAddAdder, boost::ref(ei), _1, alpha, boost::ref(x)));
  }

  /* ************************************************************************* */
  namespace {
    struct CgPixelCalculations
    {
      const VectorValues& d;
      double& AdNormSquared;
      VectorValues& AtAd;
      AccumulatorMutex& mutex1;
      AccumulatorMutex& mutex2;

      CgPixelCalculations(const VectorValues& d, double& AdNormSquared,
          VectorValues& AtAd, AccumulatorMutex& mutex1, AccumulatorMutex& mutex2) :
        d(d), AdNormSquared(AdNormSquared), AtAd(AtAd), mutex1(mutex1), mutex2(mutex2) {}

      void operator()(const GaussianFactor& factor) const
      {
        const JacobianFactor& jacobian = dynamic_cast<const JacobianFactor&>(factor);

        // Update AdNormSquared, i.e. <Ad, Ad>
        Vector OneAd = jacobian * d;
        const double OneAdSquaredNorm = OneAd.squaredNorm();
        { AccumulatorMutex::scoped_lock lock(mutex1);
        AdNormSquared += OneAdSquaredNorm; }

        // Update AtAd, i.e. A'Ad
        { AccumulatorMutex::scoped_lock lock(mutex2);
        jacobian.transposeMultiplyAdd(1.0, OneAd, AtAd); }
      }
    };
  }

  /* ************************************************************************* */
  VectorValues BagGraph::optimizeConjugateGradients(
      const VectorValues& start, double threshold, double maxIter) const
  {
    // Compute initial states
    VectorValues g = this->gradient(start);
    VectorValues d = g;
    VectorValues x = start;
    double gamma = g.dot(g);

    cout << "gamma = " << gamma << endl;

    int iteration = 0;
    while(true)
    {
      if(iteration >= maxIter)
        break;

      // Do per-pixel calculations and accumulate results
      double AdNormSquared = 0.0;
      VectorValues AtAd;
      cout << "Doing per-pixel calculations" << endl;
      {
        AccumulatorMutex mutex1, mutex2;
        foreachPixel(CgPixelCalculations(d, AdNormSquared, AtAd, mutex1, mutex2));
      }

      cout << "AdNormSquared = " << AdNormSquared << endl;

      // Calculate step
      cout << "Calculating step" << endl;
      const double alpha = - d.dot(g) / AdNormSquared;
      x += alpha * d;
      g += alpha * AtAd;
      const double gNormSquared = g.squaredNorm();

      cout << "alpha = " << alpha << ", gNormSquared = " << gNormSquared << endl;

      if(gamma < threshold)
        break;

      const double beta = gNormSquared / gamma;
      d = g + beta * d;
      gamma = gNormSquared;

      cout << "beta = " << beta << ", gamma = " << gamma << endl;

      ++ iteration;
    }

    return x;
  }

}


