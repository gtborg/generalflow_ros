/**
 * @file    learnBasisFlows.cpp
 * @brief   Learn basis flows from a ROS bag file
 * @author  Richard Roberts
 * @created Sept 17, 2013
 */

#include <iostream>
#include <fstream>
#include <vector>
#include <stdint.h>
#include <boost/foreach.hpp>
#include <boost/assign/list_of.hpp>
#include <boost/format.hpp>
#include <tbb/tbb.h>

#include <gtsam/base/Vector.h>
#include <gtsam/nonlinear/Values.h>
#include <gtsam/slam/PriorFactor.h>
#include <gtsam/linear/HessianFactor.h>
#include <gtsam/linear/GaussianFactorGraph.h>
#include <gtsam/linear/iterative.h>

#include "BasisGraph.h"
#include "learningUtils.h"
#include <generalflow_densemotion/VelocityFactor.h>

#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>

#include <opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace gtsam;
using namespace generalflow::densemotion;
using namespace generalflow::flow;
using namespace generalflow_ros;

//extern const double globalStartTime = 1373993279.449426;
//extern const double globalEndTime = 1373993287.0;
extern const double globalStartTime = 1373993305.121417;
extern const double globalEndTime = 1373993340.261431;
//extern const double globalEndTime = 1373993890.73;
const double imagePreScale = 0.0625; //0.25;
const int nthFrame = 1;

extern ofstream globalParamsFile;

/* **************************************************************************************** */
boost::shared_ptr<Values> updateVelocity(boost::shared_ptr<const rosbag::Bag> bag,
    boost::shared_ptr<const Values> velocities,
    const Matrix& basis, const Vector& sigmas, double inlierPrior)
{
  // Set up view to loop over images
  vector<string> topics;
  topics.push_back("/camera/image_raw");
  topics.push_back("camera/image_raw");
  rosbag::View view(*bag, rosbag::TopicQuery(topics), ros::Time(globalStartTime), ros::Time(globalEndTime));

  // Accumulate a graph on the velocity
  GaussianFactorGraph graph;
  SharedDiagonal unitModel = noiseModel::Unit::Create(basis.cols());

  // Loop over messages and accumulate Hessian
  ImagePairGrad imgPair;
  size_t framenum = 1;
  BOOST_FOREACH(rosbag::MessageInstance const m, view)
  {
    if(sensor_msgs::ImageConstPtr img = m.instantiate<sensor_msgs::Image>())
    {
      if(framenum % nthFrame == 0)
      {
        // Scale the image using OpenCV
        cv::Mat imgIn(img->height, img->width, CV_8UC1, const_cast<uint8_t*>(&img->data[0]));
        cv::Mat imgOut;
        cv::resize(imgIn, imgOut, cv::Size(), imagePreScale, imagePreScale, CV_INTER_AREA);

        const Eigen::Map<const Eigen::Matrix<uint8_t, -1, -1, Eigen::RowMajor> > imgM(imgOut.data, imgOut.rows, imgOut.cols);
        if(imgPair.empty())
          imgPair = ImagePairGrad(imgM.cast<double>() / 255.0, imgM.cast<double>() / 255.0);
        else
          imgPair = ImagePairGrad(imgPair, imgM.cast<double>() / 255.0);
        graph += VelocityFactor(framenum, basis, sigmas, inlierPrior, imgPair).linearize(*velocities);
        graph += PriorFactor<LieVector>(framenum, LieVector(Vector::Zero(basis.cols())), unitModel).linearize(*velocities);
      }

      ++ framenum;

      if(framenum % 100 == 0)
        cout << "Frame " << framenum << endl;
    }
  }

  // Solve for the velocity
  return boost::make_shared<Values>(velocities->retract(graph.optimize()));
}

/* **************************************************************************************** */
Matrix updateBasis(boost::shared_ptr<const rosbag::Bag> bag, boost::shared_ptr<const Values> velocities,
    const Matrix& basis, const Vector& sigmas, double inlierPrior, int iteration)
{
  // Store current basis in a Values
  Values::shared_ptr basisValues = boost::make_shared<Values>();
  for(size_t pix = 0; pix < basis.rows() / 2; ++pix)
    basisValues->insert(pix, LieMatrix(basis.block(2*pix, 0, 2, basis.cols())));

  // Create graph that will compute the gradient out-of-core
  BasisGraph basisGraph(bag, nthFrame, imagePreScale, false, velocities, basisValues, sigmas, inlierPrior);

  // Solve using conjugate gradients
  ConjugateGradientParameters params(1, 10, 11, 1e-3, 1e-3);
  VectorValues start = basisValues->zeroVectors();

  VectorValues solnVector = basisGraph.optimizeConjugateGradients(start, 1e-3, 50);
//  GaussianFactorGraph basisFactors = basisGraph.getAllFactors();
//  SharedDiagonal model = noiseModel::Isotropic::Sigma(2 * basis.cols(), 1e3);
//  for(size_t i = 0; i < basis.rows() / 2; ++i)
//    basisFactors += PriorFactor<LieMatrix>(i, LieMatrix(Matrix::Zero(2, basis.cols())), model).linearize(*basisValues);
//  VectorValues solnVector = basisFactors.optimize();

  Values soln = basisValues->retract(solnVector);

  Matrix updatedBasis(basis.rows(), basis.cols());
  // Reassemble basis
  for(size_t pix = 0; pix < basis.rows() / 2; ++pix)
    updatedBasis.block(2*pix, 0, 2, basis.cols()) = soln.at<LieMatrix>(pix);

  return updatedBasis;
}

/* **************************************************************************************** */
int main(int argc, char* argv[])
{
  //tbb::task_scheduler_init init(1);

  if(argc < 2)
  {
    cout << "USAGE:  " << argv[0] << " BAGFILE" << endl;
    return 1;
  }

  const DenseIndex q = 4;

  // Open bag file
  const string bagfile = argv[1];
  cout << "Opening " << bagfile << "..." << endl;
  boost::shared_ptr<const rosbag::Bag> bag = boost::make_shared<rosbag::Bag>(bagfile, rosbag::bagmode::Read);
  ImageInfo imageInfo = getImageInfo(bag, imagePreScale);

  // Initialize with random basis and zero velocity
  Matrix basis = Matrix::Random(2 * imageInfo.width * imageInfo.height, q);

  vector<string> topics;
  topics.push_back("/camera/image_raw");
  topics.push_back("camera/image_raw");
  rosbag::View view(*bag, rosbag::TopicQuery(topics), ros::Time(globalStartTime), ros::Time(globalEndTime));
  boost::shared_ptr<Values> velocities = boost::make_shared<Values>();
  for(size_t frame = 1; frame <= view.size(); ++frame)
  {
    if(frame % nthFrame == 0)
      velocities->insert(frame, LieVector(Vector::Zero(q)));
  }

  writeBasis(0, basis);
  writeVelocities(0, velocities);

  // Do learning iterations
  double flowSigmaV = 0.1;
  double flowSigmaF = 0.5;
  double imgSigmaV = 0.01;
  double imgSigmaF = 0.01;
  Vector sigmas(4); sigmas << flowSigmaV, flowSigmaF, imgSigmaV, imgSigmaF;
  double inlierPrior = 0.8;

  for(int iteration = 1; iteration <= 15; ++iteration)
  {
    cout << "Updating velocity" << endl;
    velocities = updateVelocity(bag, velocities, basis, sigmas, inlierPrior);
    writeVelocities(iteration, velocities);
    cout << "Updating basis" << endl;
    basis = updateBasis(bag, velocities, basis, sigmas, inlierPrior, iteration);
    writeBasis(iteration, basis);
  }
}


