/**
 * @file    BasisNGraph.cpp
 * @brief   
 * @author  Richard Roberts
 * @created Oct 6, 2013
 */

#include "BasisNGraph.h"

#include <generalflow_denselabeling/BasisNFactor.h>
#include <generalflow_denselabeling/BasisNTransFactor.h>
#include <generalflow_denselabeling/VelocityNFactor.h>
#include <boost/make_shared.hpp>
#include <gtsam/nonlinear/Symbol.h>
#include <gtsam/base/LieMatrix.h>
#include <gtsam/slam/BetweenFactor.h>
#include <gtsam/slam/PriorFactor.h>
#include <gtsam/linear/GaussianBayesTree.h>
#include <fstream>

using namespace gtsam;
using namespace std;
using namespace generalflow::flow;
using namespace generalflow::denselabeling;

ofstream globalParamsFile;

namespace generalflow_ros
{

  /* ************************************************************************* */
  boost::shared_ptr<BagGraph::FrameInfo> BasisNGraph::createFrameInfo(size_t framenum, size_t bagnum, const ros::Time& stamp,
      const generalflow::flow::ImagePairGrad::shared_ptr& imgPair) const
  {
    const DenseIndex wid = imgPair->prevFrame().cols();
    const DenseIndex hei = imgPair->prevFrame().rows();
    const DenseIndex r = basisLinearizationPoint_->at<LieMatrix>(symbol('r', 0)).cols();
    const DenseIndex q = velocities_->filter<LieVector>().begin()->value.size() - r;

    // Get number of translation flows
    DenseIndex nTrans = 0;
    for(unsigned char c = 'a'; c < 'r'; ++c)
    {
      if(basisLinearizationPoint_->exists(symbol(c, 0)))
        ++ nTrans;
      else
        break;
    }

    boost::shared_ptr<FrameInfo> frameInfo = boost::make_shared<FrameInfo>(
          framenum, iteration_ % 5 == 1 ? visualizationPublisher_ : boost::none,
          stamp, iteration_, imageInfo(), nTrans + 2);

    // Get basis
    //Matrix basis1(2*wid*hei, q + r), basis2(2*wid*hei, r);
    Matrix basisRot(2*wid*hei, r);
    vector<Matrix> basisTrans(nTrans, Matrix(2*wid*hei, q));
    for(size_t pix = 0; pix < wid*hei; ++pix)
    {
      for(DenseIndex k = 0; k < nTrans; ++k)
        basisTrans[k].middleRows(2*pix, 2) = basisLinearizationPoint_->at<LieMatrix>(symbol('a'+k, pix));
      basisRot.middleRows(2*pix, 2) = basisLinearizationPoint_->at<LieMatrix>(symbol('r', pix));
    }

    const Matrix sigmaV = velocities_->at<LieMatrix>(symbol('S', framenum));
    frameInfo->indicators = solveIndicators(
          *imgPair, basisRot, basisTrans, velocities_->at<LieVector>(framenum),
          classPriors_->at(bagnum),
          flowSigmas_, pixSigmas_,
          sigmaV,
          PIXELS,
          frameInfo->visualization);

    // Solve velocity
    //cout << "Solving velocity frame " << framenum << endl;
    GaussianFactorGraph velocityGraph;
    velocityGraph += VelocityNFactor(framenum, basisRot, basisTrans, flowSigmas_, pixSigmas_,
      frameInfo->indicators, imgPair).linearize(*velocities_);
    //velocityGraph += Velocity2Factor(framenum, basis1, basis2, sigmas_, classPriors_, imgPair).linearize(*velocities_);
    SharedDiagonal priorModel = noiseModel::Isotropic::Sigma(q+r, 1e4);
    velocityGraph += PriorFactor<LieVector>(framenum, LieVector(Vector::Zero(q+r)), priorModel).linearize(*velocities_);
    GaussianBayesTree::shared_ptr R = velocityGraph.eliminateMultifrontal(boost::none, EliminateQR);
    VectorValues update = R->optimize();
    LieVector newVelocity = velocities_->at<LieVector>(framenum).retract(update.at(framenum));
    LieMatrix covV = R->marginalCovariance(framenum);
    velocities_->update(framenum, newVelocity);
    velocities_->update(symbol('S', framenum), covV);
    frameInfo->velocity = newVelocity;

    return frameInfo;
  }

  /* ************************************************************************* */
  GaussianFactorGraph BasisNGraph::createStaticFactors() const
  {
    const DenseIndex wid = imageInfo().width;
    const DenseIndex hei = imageInfo().height;
    const double betweenSigma = 2.0 * imagePreScale_;
    const double betweenPrec = 1.0 / (betweenSigma * betweenSigma);

    // Get number and dimension of translation flows
    DenseIndex nTrans = 0;
    vector<DenseIndex> q;
    vector<SharedDiagonal> noiseT;
    for(unsigned char c = 'a'; c < 'r'; ++c)
    {
      if(basisLinearizationPoint_->exists(symbol(c, 0)))
      {
        ++ nTrans;
        q.push_back(basisLinearizationPoint_->at<LieMatrix>(symbol(c, 0)).cols());
        noiseT.push_back(noiseModel::Isotropic::Sigma(2*q.back(), betweenSigma));
      }
    }

    const DenseIndex r = basisLinearizationPoint_->at<LieMatrix>(symbol('r', 0)).cols();
    SharedDiagonal noiseR = noiseModel::Isotropic::Sigma(2*r, betweenSigma);

    NonlinearFactorGraph graph;
    graph.reserve(wid*hei*2*(nTrans + 1));

    static bool wroteParams = false;
    if(!wroteParams)
    {
      globalParamsFile << "betweenSigma = " << betweenSigma << endl;
      wroteParams = true;
    }
    for(DenseIndex j = 0; j < wid; ++j) {
      for(DenseIndex i = 0; i < hei; ++i) {
        size_t pix = j*hei + i;
        size_t pixR = pix + hei;
        size_t pixD = pix + 1;
        if(j != wid - 1)
        {
          if(updateRotationBasis_)
            graph += BetweenFactor<LieMatrix>(symbol('r', pix), symbol('r', pixR),
            LieMatrix(Matrix::Zero(2, r)), noiseR);
          for(DenseIndex k = 0; k < nTrans; ++k)
            graph += BetweenFactor<LieMatrix>(symbol('a' + k, pix), symbol('a' + k, pixR),
            LieMatrix(Matrix::Zero(2, q[k])), noiseT[k]);
        }
        if(i != hei - 1)
        {
          if(updateRotationBasis_)
            graph += BetweenFactor<LieMatrix>(symbol('r', pix), symbol('r', pixD),
            LieMatrix(Matrix::Zero(2, r)), noiseR);
          for(DenseIndex k = 0; k < nTrans; ++k)
            graph += BetweenFactor<LieMatrix>(symbol('a' + k, pix), symbol('a' + k, pixD),
            LieMatrix(Matrix::Zero(2, q[k])), noiseT[k]);

        }
      }
    }
    return *graph.linearize(*basisLinearizationPoint_);
  }

  /* ************************************************************************* */
  void BasisNGraph::createPixelFactors(const boost::shared_ptr<Base::FrameInfo>& baseFrameInfo,
      const tbb::blocked_range<size_t>& pixelIndexRange,
      const boost::shared_ptr<generalflow::flow::ImagePairGrad>& imgPair,
      gtsam::GaussianFactorGraph::iterator firstFactorPtr,
      gtsam::GaussianFactorGraph::iterator lastFactorPtr) const
  {
    const FrameInfo& frameInfo = static_cast<const FrameInfo&>(*baseFrameInfo);
    vector<Key> basesTransKeys(flowSigmas_.size() - 2);
    for(size_t pix = pixelIndexRange.begin(); pix != pixelIndexRange.end(); ++pix)
    {
      for(DenseIndex k = 0; k < basesTransKeys.size(); ++k)
        basesTransKeys[k] = symbol('a' + k, pix);
      DenseIndex i = pix % imgPair->curFrame().rows();
      DenseIndex j = pix / imgPair->curFrame().rows();
      //cout << frameInfo.indicators.col(pix).transpose() << endl;
      if(updateRotationBasis_)
      {
        *(firstFactorPtr ++) = BasisNFactor(
          symbol('r', pix), basesTransKeys,
          frameInfo.velocity, flowSigmas_, pixSigmas_,
          frameInfo.indicators.col(pix), imgPair, j, i, frameInfo.visualization)
          .linearize(*basisLinearizationPoint_);
      }
      else
      {
        *(firstFactorPtr ++) = BasisNTransFactor(
          symbol('r', pix), basesTransKeys,
          frameInfo.velocity, flowSigmas_, pixSigmas_,
          frameInfo.indicators.col(pix), imgPair, j, i, frameInfo.visualization)
          .linearize(*basisLinearizationPoint_);
      }
    }
  }
}




