/**
 * @file    learnTemplates.cpp
 * @brief   
 * @author  Richard Roberts
 * @created Oct 5, 2013
 */

#include <sys/resource.h>

#include <iostream>
#include <fstream>
#include <vector>
#include <stdint.h>
#include <boost/foreach.hpp>
#include <boost/assign/list_of.hpp>
#include <boost/format.hpp>
#include <boost/random.hpp>
#include <boost/static_assert.hpp>
#include <boost/type_traits/is_same.hpp>
#include <boost/thread.hpp>
#include <tbb/tbb.h>

#include <gtsam/base/Vector.h>
#include <gtsam/nonlinear/Values.h>
#include <gtsam/nonlinear/Symbol.h>
#include <gtsam/slam/PriorFactor.h>
#include <gtsam/linear/HessianFactor.h>
#include <gtsam/linear/GaussianFactorGraph.h>
#include <gtsam/linear/GaussianBayesTree.h>
#include <gtsam/linear/iterative.h>

#include "learningUtils.h"
#include "rosTransforms.h"
#include <generalflow_rosmsgs/FullImageErrorVisualization.h>
#include <generalflow_denselabeling/VelocityNFactor.h>

#include <ros/ros.h>
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>

#include <opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace gtsam;
using namespace generalflow::denselabeling;
using namespace generalflow::flow;
using namespace generalflow_ros;

//extern const double globalStartTime = 1373993279.449426;
//extern const double globalEndTime = 1373993287.0;
//extern const double globalStartTime = 1373993305.121417;
//extern const double globalEndTime = 1373993340.0;
//extern const double globalEndTime = 1373993890.73;
const double imagePreScale = 0.5;
const ImageROI roi(0, 0, 512, 336);
const int aboveHorizon = imagePreScale * 210;
const int horizonHalfMargin = imagePreScale * 20;
const int nthFrame = 5;
ofstream globalParamsFile("params.txt");

//tbb::task_scheduler_init init(1);

/* **************************************************************************************** */
pair<Matrix, vector<Matrix> > loadBases(const std::string& filename)
{
  rosbag::Bag bag(filename);

  vector<string> topics;
  topics.push_back("/learnTemplates/learnRotationBasis/basis");
  topics.push_back("/learnTemplates/learnTranslationBasis/basis");
  topics.push_back("/learnTemplates/learnJointBasis/basis");
  rosbag::View view(bag, rosbag::TopicQuery(topics));

  generalflow_rosmsgs::BasisUpdateConstPtr lastBasisUpdate;
  BOOST_FOREACH(const rosbag::MessageInstance m, view)
  {
    if(generalflow_rosmsgs::BasisUpdateConstPtr basisUpdate = m.instantiate<generalflow_rosmsgs::BasisUpdate>())
      lastBasisUpdate = basisUpdate;
  }

  if(lastBasisUpdate)
  {
    BOOST_STATIC_ASSERT((boost::is_same<generalflow_rosmsgs::Basis::_coeffs_type::value_type, Matrix::Scalar>::value));
    Matrix basisRot = Eigen::Map<const Matrix>(&lastBasisUpdate->basisRot.coeffs[0],
        lastBasisUpdate->basisRot.rows, lastBasisUpdate->basisRot.cols);
    std::vector<Matrix> basesTrans;
    BOOST_FOREACH(const generalflow_rosmsgs::Basis& basisTrans, lastBasisUpdate->basesTrans)
    {
      basesTrans.push_back(Eigen::Map<const Matrix>(&basisTrans.coeffs[0], basisTrans.rows, basisTrans.cols));
    }

    return make_pair(basisRot, basesTrans);
  }
  else
  {
    cout << "Input file does not seem to be a learning log - does not contain any basis update messages." << endl;
    exit(1);
  }
}

/* **************************************************************************************** */
void updateSolution(const boost::shared_ptr<ImagePairGrad>& imgPair,
    Vector& velocity, Matrix& sigmaV, Matrix& indicators, const Matrix& basisRot,
    const vector<Matrix>& basesTrans, const Vector& flowSigmas, const Vector& pixSigmas,
    const boost::shared_ptr<vector<Matrix> >& classPriors, int iteration, int framenum,
    boost::optional<const ros::Publisher&> visualizationPublisher, const ros::Time& stamp)
{
  const DenseIndex wid = imgPair->prevFrame().cols();
  const DenseIndex hei = imgPair->prevFrame().rows();

  boost::shared_ptr<generalflow::flow::FullImageErrorVisualization> visualization;
  if(visualizationPublisher)
    visualization = boost::make_shared<generalflow::flow::FullImageErrorVisualization>(
    iteration, framenum, wid, hei, basesTrans.size() + 2);
  indicators = solveIndicators(*imgPair, basisRot, basesTrans, velocity,
      *classPriors,
      flowSigmas, pixSigmas,
      sigmaV,
      SUPERPIXELS,
      visualization);

  // Solve velocity
  GaussianFactorGraph velocityGraph;
  Values velocityValues;
  velocityValues.insert(framenum, LieVector(velocity));
  velocityGraph += VelocityNFactor(
    framenum, basisRot, basesTrans, flowSigmas, pixSigmas,
    indicators, imgPair, visualization).linearize(velocityValues);
  //SharedDiagonal priorModel = noiseModel::Isotropic::Sigma(q+r, 1e4);
  //velocityGraph += PriorFactor<LieVector>(framenum, LieVector(Vector::Zero(q+r)), priorModel).linearize(*velocities_);
  GaussianBayesTree::shared_ptr R = velocityGraph.eliminateMultifrontal(boost::none, EliminateQR);
  VectorValues update = R->optimize();
  velocity = LieVector(velocity).retract(update.at(framenum));
  sigmaV = R->marginalCovariance(framenum);

  if(visualizationPublisher)
    visualizationPublisher->publish(FullImageErrorVisualizationToRos(*visualization, stamp));
}

/* **************************************************************************************** */
int main(int argc, char* argv[])
{
  //struct rlimit64 limit = { 4000000000, 4000000000 };
  //setrlimit64(RLIMIT_DATA, &limit);
  //setrlimit64(RLIMIT_AS, &limit);

  //tbb::task_scheduler_init init(1);

  const int nIterations = 3;

  if(argc < 2)
  {
    cout << "USAGE:  " << argv[0] << " LEARNING_LOG.bag (BAGFILE )+" << endl;
    return 1;
  }

  ros::init(argc, argv, "runInference");
  ros::NodeHandle node;
  ros::Publisher visualizationPublisher =
    node.advertise<generalflow_rosmsgs::FullImageErrorVisualization>(
    "runInference/visualization", 1000);
  ros::Publisher basisPublisher =
    node.advertise<generalflow_rosmsgs::BasisUpdate>(
    "runInference/basis", 1000);
  ros::Publisher velocityPublisher =
    node.advertise<generalflow_rosmsgs::VelocityUpdate>(
    "runInference/velocity", 1000);

  // Give rosbag time to catch up
  boost::this_thread::sleep(boost::posix_time::milliseconds(1500));

  // Load bases
  Matrix basisRot;
  vector<Matrix> basesTrans;
  boost::tie(basisRot, basesTrans) = loadBases(argv[1]);
  basesTrans.push_back(basesTrans[0] * 1.1);

  const DenseIndex nTransBases = basesTrans.size();
  const DenseIndex K = 2 + nTransBases;
  const DenseIndex q = basesTrans.front().cols();
  const DenseIndex r = basisRot.cols();
  globalParamsFile << "q = " << q << "\n" << "r = " << r << endl;
  globalParamsFile << "imagePreScale = " << imagePreScale << endl;
  globalParamsFile << "nthFrame = " << nthFrame << endl;

  // Open bag files
  vector<boost::shared_ptr<const rosbag::Bag> > bags;
  for(int i = 2; i < argc; ++i)
  {
    const string bagfile = argv[i];
    cout << "Opening " << bagfile << "..." << endl;
    bags.push_back(boost::make_shared<rosbag::Bag>(bagfile, rosbag::bagmode::Read));
  }
  ImageInfo imageInfo = getImageInfo(bags.front(), imagePreScale, roi);

  // Output bases to log file
  for(int iteration = 1; iteration <= nIterations; ++iteration)
    basisPublisher.publish(BasisUpdateToRos(imageInfo, basisRot, basesTrans, iteration));

  // Parameters
  const double flowSigmaOut = 1.0 * imagePreScale;
  const Vector flowSigmasTrans = (Vector(2) << 0.35 * imagePreScale, 0.35 * imagePreScale);
  const double flowSigmaRot = 0.35 * imagePreScale;
  const double pixSigmaOut = 0.02;
  const Vector pixSigmasTrans = (Vector(2) << 0.02, 0.02);
  const double pixSigmaRot = 0.02;

  const Vector flowSigmas = (Vector(2 + nTransBases) <<
    flowSigmaOut, flowSigmasTrans, flowSigmaRot);
  const Vector pixSigmas = (Vector(2 + nTransBases) <<
    pixSigmaOut, pixSigmasTrans, pixSigmaRot);

  // Set up class priors
  const double outlierP = 0.05;// 0.15;
  const double outOfClassP = 0.00001;
  const double transInlierOdds = 1.05;
  const double transOutlierP = (1.0 - outlierP) / (transInlierOdds + 1) - (outOfClassP / 2.0);
  const double transInlierP = (1.0 - outlierP) / (1.0 / transInlierOdds + 1) - (outOfClassP / 2.0);
  const double mixedOutlierP = (1.0 - outlierP) / (2.0 * transInlierOdds + 1) - (outOfClassP / 3.0);
  const double mixedInlierP = 0.5 * ((1.0 - outlierP) / (1.0 / (2.0 * transInlierOdds) + 1) - (outOfClassP / 3.0));

  cout << "transOutlierP = " << transOutlierP << endl;
  cout << "transInlierP = " << transInlierP << endl;

  // Class priors
  boost::shared_ptr<vector<Matrix> > classPriors = boost::make_shared<vector<Matrix> >(
    2 + nTransBases, Matrix(imageInfo.height, imageInfo.width));

  // Rotation/translation data
  classPriors->at(0).setConstant(outlierP); // Outlier
  classPriors->at(1).topRows(aboveHorizon - horizonHalfMargin).setConstant(outOfClassP); // Ground plane
  classPriors->at(2).topRows(aboveHorizon - horizonHalfMargin).setConstant(transOutlierP); // Obstable
  classPriors->at(3).topRows(aboveHorizon - horizonHalfMargin).setConstant(transInlierP); // Rotation
  classPriors->at(1).middleRows(aboveHorizon - horizonHalfMargin, 2 * horizonHalfMargin).setConstant(mixedInlierP); // Ground plane
  classPriors->at(2).middleRows(aboveHorizon - horizonHalfMargin, 2 * horizonHalfMargin).setConstant(mixedOutlierP); // Obstacle
  classPriors->at(3).middleRows(aboveHorizon - horizonHalfMargin, 2 * horizonHalfMargin).setConstant(mixedInlierP); // Rotation
  classPriors->at(1).bottomRows(imageInfo.height - aboveHorizon - horizonHalfMargin).setConstant(transInlierP); // Ground plane
  classPriors->at(2).bottomRows(imageInfo.height - aboveHorizon - horizonHalfMargin).setConstant(transOutlierP); // Obstacle
  classPriors->at(3).bottomRows(imageInfo.height - aboveHorizon - horizonHalfMargin).setConstant(outOfClassP); // Rotation

//  // Set up class priors
//  const double outlierP = 0.05;// 0.15;
//  const double transOutlierP = 0.3;
//  const Vector inlierTransP = (Vector(2) << (1.0 - outlierP - transOutlierP) / 2.0, transOutlierP); // Vector::Constant(2, (1.0 - outlierP) / 3.0); // (Vector(2) << 0.3, 0.175);
//  const double inlierRotP = (1.0 - outlierP - transOutlierP) / 2.0;

  // Initialize parameters
//  boost::shared_ptr<vector<Matrix> > classPriors = boost::make_shared<vector<Matrix> >(
//    2 + nTransBases, Matrix(imageInfo.height, imageInfo.width));
//  classPriors->at(0).setConstant(outlierP);
//  for(DenseIndex k = 1; k <= nTransBases; ++k)
//    classPriors->at(k).setConstant(inlierTransP(k-1));
//  classPriors->back().setConstant(inlierRotP);

  vector<string> topics;
  topics.push_back("/camera/image_raw");
  topics.push_back("camera/image_raw");

  size_t framenum = 1;
  BOOST_FOREACH(const boost::shared_ptr<const rosbag::Bag>& bag, bags)
  {
    rosbag::View view(*bag, rosbag::TopicQuery(topics));
    Vector velocity = Vector::Zero(q + r);
    Matrix sigmaV = Matrix::Identity(q + r, q + r);
    Matrix indicators = Matrix::Constant(K, imageInfo.width * imageInfo.height, 1.0 / double(K));
    boost::shared_ptr<ImagePairGrad> imgPair;
    BOOST_FOREACH(rosbag::MessageInstance const m, view)
    {
      if(sensor_msgs::ImageConstPtr img = m.instantiate<sensor_msgs::Image>())
      {
        // Scale the image using OpenCV to be sure we get the correct rounding
        cv::Mat imgIn(img->height, img->width, CV_8UC1, const_cast<uint8_t*>(&img->data[0]));
        if(roi.x >= 0)
          imgIn(cv::Rect(roi.x, roi.y, roi.w, roi.h)).copyTo(imgIn);
        cv::Mat imgOut;
        cv::resize(imgIn, imgOut, cv::Size(), imagePreScale, imagePreScale, CV_INTER_AREA);

        // Get color image
        cv::Mat imgColor;
        cv::cvtColor(imgOut, imgColor, CV_GRAY2RGB);
        Eigen::Matrix<uint8_t, Eigen::Dynamic, Eigen::Dynamic> imgC(imgOut.rows * 3, imgOut.cols);
        for(DenseIndex x = 0; x < imgOut.cols; ++x)
          for(DenseIndex y = 0; y < imgOut.rows; ++y)
            imgC.col(x).segment(3*y, 3) = Eigen::Map<Eigen::Matrix<uint8_t, 3, 1> >(imgColor.at<cv::Vec3b>(y, x).val);

        const Eigen::Map<const Eigen::Matrix<uint8_t, -1, -1, Eigen::RowMajor> > imgM(
              imgOut.data, imgOut.rows, imgOut.cols);
        if(!imgPair)
          imgPair = boost::make_shared<ImagePairGrad>(imgM.cast<double>() / 255.0, imgM.cast<double>() / 255.0, true, imgC);
        else
          imgPair = boost::make_shared<ImagePairGrad>(*imgPair, imgM.cast<double>() / 255.0, imgC);

        if(framenum % nthFrame == 0)
        {
          for(int iteration = 1; iteration <= nIterations; ++iteration)
          {
            updateSolution(imgPair, velocity, sigmaV, indicators, basisRot, basesTrans,
              flowSigmas, pixSigmas, classPriors, iteration, framenum, visualizationPublisher, img->header.stamp);
            Values velocities;
            velocities.insert(framenum, LieVector(velocity));
            velocityPublisher.publish(VelocityUpdateToRos(velocities, 0, iteration));
          }
          cout << "Frame " << framenum << endl;
        }
        ++ framenum;
      }
    }
  }

  return 0;
}






