/**
 * @file    BasisNGraph.h
 * @brief   
 * @author  Richard Roberts
 * @created Oct 6, 2013
 */

#pragma once

#include "BagGraph.h"
#include "rosTransforms.h"

#include <map>
#include <boost/thread.hpp>
#include <boost/make_shared.hpp>
#include <tbb/tbb.h>

#include <ros/ros.h>

#include <generalflow_denselabeling/types.h>
#include <generalflow_flow/flowOps.h>

namespace generalflow_ros
{

  /* ************************************************************************* */
  class generalflow_ros_EXPORT BasisNGraph : public BagGraph
  {
  protected:
    bool updateRotationBasis_;
    boost::shared_ptr<gtsam::Values> velocities_;
    boost::shared_ptr<const gtsam::Values> basisLinearizationPoint_;
    gtsam::Vector flowSigmas_;
    gtsam::Vector pixSigmas_;
    boost::shared_ptr<const std::vector<std::vector<gtsam::Matrix> > > classPriors_;

    boost::optional<ros::Publisher> visualizationPublisher_;
    int iteration_;

  public:

    typedef BagGraph Base;

    BasisNGraph(const std::vector<boost::shared_ptr<const rosbag::Bag> >& bags,
                int nthFrame, double imagePreScale, const ImageROI& roi, bool merge,
      bool updateRotationBasis,
      const boost::shared_ptr<gtsam::Values>& velocities,
      const boost::shared_ptr<const gtsam::Values>& basisLinearizationPoint,
      const gtsam::Vector& flowSigmas, const gtsam::Vector& pixSigmas,
      const boost::shared_ptr<const std::vector<std::vector<gtsam::Matrix> > >& classPriors,
      const boost::optional<ros::NodeHandle&>& visualizationNode = boost::none, int iteration = -1) :
      Base(bags, nthFrame, imagePreScale, roi, merge), updateRotationBasis_(updateRotationBasis),
      velocities_(velocities), basisLinearizationPoint_(basisLinearizationPoint),
      flowSigmas_(flowSigmas), pixSigmas_(pixSigmas), classPriors_(classPriors), iteration_(iteration)
    {
      if(visualizationNode)
      {
        visualizationPublisher_.reset(
            visualizationNode->advertise<generalflow_rosmsgs::FullImageErrorVisualization>(
                "BasisNGraph/visualization", 100));
      }
    }

    struct FrameInfo : Base::FrameInfo
    {
      gtsam::LieVector velocity;
      gtsam::Matrix indicators;
      boost::shared_ptr<generalflow::flow::FullImageErrorVisualization> visualization;
      boost::optional<ros::Publisher> visualizationPublisher;
      ros::Time stamp;

      FrameInfo(size_t framenum, const boost::optional<ros::Publisher>& visualizationPublisher, const ros::Time& stamp,
        int iteration, const ImageInfo& imageInfo, const gtsam::DenseIndex K) :
        Base::FrameInfo(framenum), visualizationPublisher(visualizationPublisher), stamp(stamp)
      {
        if(visualizationPublisher)
          visualization = boost::make_shared<generalflow::flow::FullImageErrorVisualization>(
              iteration, framenum, imageInfo.width, imageInfo.height, K);
      }

      virtual ~FrameInfo()
      {
        if(visualization && visualizationPublisher)
          visualizationPublisher->publish(FullImageErrorVisualizationToRos(*visualization, stamp));
      }
    };

    virtual boost::shared_ptr<Base::FrameInfo> createFrameInfo(size_t framenum, size_t bagnum, const ros::Time& stamp,
        const generalflow::flow::ImagePairGrad::shared_ptr& imgPair) const;

    virtual void createPixelFactors(const boost::shared_ptr<Base::FrameInfo>& frameInfo,
        const tbb::blocked_range<size_t>& pixelIndexRange,
        const boost::shared_ptr<generalflow::flow::ImagePairGrad>& imgPair,
        gtsam::GaussianFactorGraph::iterator firstFactorPtr,
        gtsam::GaussianFactorGraph::iterator lastFactorPtr) const;

    virtual gtsam::GaussianFactorGraph createStaticFactors() const;

    const boost::shared_ptr<gtsam::Values>& velocities() const { return velocities_; }
    const boost::shared_ptr<const gtsam::Values>& basisLinearizationPoint() const { return basisLinearizationPoint_; }
    const gtsam::Vector& flowSigmas() const { return flowSigmas_; }
    const gtsam::Vector& pixSigmas() const { return pixSigmas_; }
    const boost::shared_ptr<const std::vector<std::vector<gtsam::Matrix> > >& classPriors() const { return classPriors_; }
  };

}

