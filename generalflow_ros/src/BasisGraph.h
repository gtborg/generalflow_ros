/**
 * @file    BasisGraph.h
 * @brief   An out-of-core graph for solving for basis flows
 * @author  Richard Roberts
 */

#pragma once

#include "BagGraph.h"

#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>

namespace generalflow_ros
{

  class generalflow_ros_EXPORT BasisGraph : public BagGraph
  {
  protected:
    boost::shared_ptr<const gtsam::Values> velocities_;
    boost::shared_ptr<const gtsam::Values> basisLinearizationPoint_;
    gtsam::Vector sigmas_;
    double inlierPrior_;

    boost::optional<ros::Publisher> visualizationPublisher_;
    int iteration_;

  public:

    typedef BagGraph Base;

    BasisGraph(const boost::shared_ptr<const rosbag::Bag>& bag, int nthFrame, double imagePreScale, bool merge,
        const boost::shared_ptr<const gtsam::Values>& velocities,
        const boost::shared_ptr<const gtsam::Values>& basisLinearizationPoint,
        const gtsam::Vector& sigmas, double inlierPrior) :
          Base(bag, nthFrame, imagePreScale, merge), velocities_(velocities), basisLinearizationPoint_(basisLinearizationPoint),
          sigmas_(sigmas), inlierPrior_(inlierPrior) {}

    struct FrameInfo : Base::FrameInfo
    {
      const gtsam::LieVector& velocity;
      FrameInfo(size_t framenum, const gtsam::LieVector& velocity) :
        Base::FrameInfo(framenum), velocity(velocity) {}
    };

    virtual boost::shared_ptr<Base::FrameInfo> createFrameInfo(size_t framenum) const;

    virtual void createPixelFactors(const boost::shared_ptr<Base::FrameInfo>& frameInfo,
        const tbb::blocked_range<size_t>& pixelIndexRange,
        const boost::shared_ptr<generalflow::flow::ImagePairGrad>& imgPair,
        gtsam::GaussianFactorGraph::iterator firstFactorPtr,
        gtsam::GaussianFactorGraph::iterator lastFactorPtr) const;

    const boost::shared_ptr<const gtsam::Values>& velocities() const { return velocities_; }
    const boost::shared_ptr<const gtsam::Values>& basisLinearizationPoint() const { return basisLinearizationPoint_; }
    const gtsam::Vector& sigmas() const { return sigmas_; }
    double inlierPrior() const { return inlierPrior_; }
  };

}

