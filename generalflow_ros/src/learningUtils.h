/**
 * @file    learningUtils.h
 * @brief   
 * @author  Richard Roberts
 * @created Oct 5, 2013
 */

#pragma once

#include <boost/shared_ptr.hpp>
#include <rosbag/bag.h>
#include <gtsam/base/Matrix.h>
#include <gtsam/nonlinear/Values.h>

#include "dllexport.h"

namespace generalflow_ros
{

  struct ImageInfo {
    int width;
    int height;
  };

  struct ImageROI {
    int x;
    int y;
    int w;
    int h;

    ImageROI() :
      x(-1), y(-1), w(-1), h(-1) {}

    ImageROI(int x, int y, int w, int h) :
      x(x), y(y), w(w), h(h) {}
  };

  generalflow_ros_EXPORT
  ImageInfo getImageInfo(boost::shared_ptr<const rosbag::Bag> bag, double imagePreScale,
                         const ImageROI& roi);

  generalflow_ros_EXPORT
  void writeBasis(int iteration, const gtsam::Matrix& basis);

  generalflow_ros_EXPORT
  void writeVelocities(int iteration, boost::shared_ptr<const gtsam::Values> values);

  generalflow_ros_EXPORT
  gtsam::Values basisToValues(unsigned char keyChar, const gtsam::Matrix& basis);

  generalflow_ros_EXPORT
  gtsam::Matrix basisFromValues(unsigned char keyChar, const gtsam::Values& values);

}
