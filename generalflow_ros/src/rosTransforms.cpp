/**
 * @file    rosTransforms.cpp
 * @brief   Utilities for converting to and from ROS messages
 * @author  Richard Roberts
 * @created Dec 17, 2013
 */

#include "rosTransforms.h"

#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>

#include <boost/static_assert.hpp>
#include <boost/type_traits/is_same.hpp>

#include <gtsam/inference/Symbol.h>

using namespace generalflow_rosmsgs;

namespace generalflow_ros
{

  /* **************************************************************************************** */
  FullImageErrorVisualizationConstPtr FullImageErrorVisualizationToRos(
    const generalflow::flow::FullImageErrorVisualization& visualization, const ros::Time& stamp)
  {
    FullImageErrorVisualizationPtr visMsg = boost::make_shared<FullImageErrorVisualization>();

    visMsg->header.stamp = stamp;

    visMsg->iteration = visualization.iteration;
    visMsg->framenum = visualization.framenum;

    visMsg->prevFrame = *cv_bridge::CvImage(visMsg->header,
      sensor_msgs::image_encodings::TYPE_32FC1, visualization.prevFrame).toImageMsg();
    visMsg->curFrame = *cv_bridge::CvImage(visMsg->header,
      sensor_msgs::image_encodings::TYPE_32FC1, visualization.curFrame).toImageMsg();
    visMsg->temporalDeriv = *cv_bridge::CvImage(visMsg->header,
      sensor_msgs::image_encodings::TYPE_32FC1, visualization.temporalDeriv).toImageMsg();
    visMsg->errs = *cv_bridge::CvImage(visMsg->header,
      sensor_msgs::image_encodings::TYPE_32FC1, visualization.errs).toImageMsg();
    visMsg->normalizedErrs = *cv_bridge::CvImage(visMsg->header,
      sensor_msgs::image_encodings::TYPE_32FC1, visualization.normalizedErrs).toImageMsg();
    visMsg->invSigmas = *cv_bridge::CvImage(visMsg->header,
      sensor_msgs::image_encodings::TYPE_32FC1, visualization.invSigmas).toImageMsg();
    visMsg->indicatorEvidence = *cv_bridge::CvImage(visMsg->header,
      sensor_msgs::image_encodings::TYPE_32FC1, visualization.indicatorEvidence).toImageMsg();
    visMsg->indicatorSmoothness = *cv_bridge::CvImage(visMsg->header,
      sensor_msgs::image_encodings::TYPE_32FC1, visualization.indicatorSmoothness).toImageMsg();
    visMsg->indicators = *cv_bridge::CvImage(visMsg->header,
      sensor_msgs::image_encodings::TYPE_32FC1, visualization.indicators).toImageMsg();
    visMsg->expectedFlow = *cv_bridge::CvImage(visMsg->header,
      sensor_msgs::image_encodings::TYPE_32FC1, visualization.expectedFlow).toImageMsg();

    return visMsg;
  }

  /* **************************************************************************************** */
  BasisUpdateConstPtr BasisUpdateToRos(const ImageInfo& imageInfo,
    const gtsam::Matrix& basisRot, const std::vector<gtsam::Matrix>& basesTrans, int iteration)
  {
    BasisUpdatePtr basisMsg = boost::make_shared<BasisUpdate>();

    basisMsg->header.stamp = ros::Time::now();

    basisMsg->iteration = iteration;
    basisMsg->imgWidth = imageInfo.width;
    basisMsg->imgHeight = imageInfo.height;

    // Make sure that the message uses the same scalar type as Matrix
    BOOST_STATIC_ASSERT(
      (boost::is_same<Matrix::Scalar, BasisUpdate::_basisRot_type::_coeffs_type::value_type>::value));

    basisMsg->basisRot.rows = basisRot.rows();
    basisMsg->basisRot.cols = basisRot.cols();
    basisMsg->basisRot.coeffs.resize(basisRot.size());
    Eigen::Map<Matrix>(&basisMsg->basisRot.coeffs[0], basisRot.rows(), basisRot.cols()) =
      basisRot;

    basisMsg->basesTrans.resize(basesTrans.size());
    for(size_t k = 0; k < basesTrans.size(); ++k)
    {
      basisMsg->basesTrans[k].rows = basesTrans[k].rows();
      basisMsg->basesTrans[k].cols = basesTrans[k].cols();
      basisMsg->basesTrans[k].coeffs.resize(basesTrans[k].size());
      Eigen::Map<Matrix>(&basisMsg->basesTrans[k].coeffs[0], basesTrans[k].rows(), basesTrans[k].cols()) =
        basesTrans[k];
    }

    return basisMsg;
  }

  /* **************************************************************************************** */
  VelocityUpdateConstPtr VelocityUpdateToRos(
    const gtsam::Values& velocityValues, unsigned char keyChar, int iteration)
  {
    VelocityUpdatePtr velocityMsg = boost::make_shared<VelocityUpdate>();

    velocityMsg->header.stamp = ros::Time::now();

    velocityMsg->iteration = iteration;

    // Count frames
    DenseIndex nFrames = 0;
    for(Values::const_iterator it = velocityValues.lower_bound(symbol(keyChar, 0));
      it != velocityValues.end() && symbolChr(it->key) == keyChar; ++it)
      ++ nFrames;

    // Make sure that the message uses the same scalar type as Matrix
    BOOST_STATIC_ASSERT(
      (boost::is_same<Vector::Scalar, VelocityFrame::_velocity_type::value_type>::value));

    // Allocate frames
    velocityMsg->velocities.reserve(nFrames);
    for(Values::const_iterator it = velocityValues.lower_bound(symbol(keyChar, 0));
      it != velocityValues.end() && symbolChr(it->key) == keyChar; ++it)
    {
      velocityMsg->velocities.push_back(VelocityFrame());
      velocityMsg->velocities.back().frame = symbolIndex(it->key);
      const LieVector& vel = dynamic_cast<const LieVector&>(it->value);
      velocityMsg->velocities.back().velocity.resize(vel.size());
      Eigen::Map<Vector>(&velocityMsg->velocities.back().velocity[0], vel.size()) = vel;
    }

    return velocityMsg;
  }


}
