/**
 * @file    BasisGraph.cpp
 * @brief   An out-of-core graph for solving for basis flows
 * @author  Richard Roberts
 */

#include "BasisGraph.h"

#include <generalflow_densemotion/BasisFactor.h>
#include <boost/make_shared.hpp>

using namespace gtsam;
using namespace std;
using namespace generalflow::flow;
using namespace generalflow::densemotion;

namespace generalflow_ros
{
  /* ************************************************************************* */
  boost::shared_ptr<BagGraph::FrameInfo> BasisGraph::createFrameInfo(size_t framenum) const
  {
    return boost::make_shared<FrameInfo>(framenum, velocities_->at<LieVector>(framenum));
  }

  /* ************************************************************************* */
  void BasisGraph::createPixelFactors(const boost::shared_ptr<Base::FrameInfo>& baseFrameInfo,
      const tbb::blocked_range<size_t>& pixelIndexRange,
      const boost::shared_ptr<generalflow::flow::ImagePairGrad>& imgPair,
      gtsam::GaussianFactorGraph::iterator firstFactorPtr,
      gtsam::GaussianFactorGraph::iterator lastFactorPtr) const
  {
    const FrameInfo& frameInfo = static_cast<const FrameInfo&>(*baseFrameInfo);
    for(size_t pix = pixelIndexRange.begin(); pix != pixelIndexRange.end(); ++pix)
    {
      DenseIndex i = pix % imgPair->curFrame().rows();
      DenseIndex j = pix / imgPair->curFrame().rows();
      *(firstFactorPtr ++) = BasisFactor(pix, frameInfo.velocity, sigmas_, inlierPrior_, imgPair, j, i)
          .linearize(*basisLinearizationPoint_);
    }
  }
}

