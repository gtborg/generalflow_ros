/**
 * @file    learnTemplates.cpp
 * @brief   
 * @author  Richard Roberts
 * @created Oct 5, 2013
 */

#include <sys/resource.h>

#include <tbb/tbb.h>
//tbb::task_scheduler_init init(1);

#include <iostream>
#include <fstream>
#include <vector>
#include <stdint.h>
#include <boost/foreach.hpp>
#include <boost/assign/list_of.hpp>
#include <boost/format.hpp>
#include <boost/random.hpp>

#include <gtsam/base/Vector.h>
#include <gtsam/nonlinear/Values.h>
#include <gtsam/nonlinear/Symbol.h>
#include <gtsam/slam/PriorFactor.h>
#include <gtsam/linear/HessianFactor.h>
#include <gtsam/linear/GaussianFactorGraph.h>
#include <gtsam/linear/iterative.h>

#include "BasisNGraph.h"
#include "learningUtils.h"
#include <generalflow_denselabeling/VelocityNFactor.h>

#include <ros/ros.h>
#include <rosbag/bag.h>
#include <rosbag/view.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>

#include <opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace gtsam;
using namespace generalflow::denselabeling;
using namespace generalflow::flow;
using namespace generalflow_ros;

// For auto rally data
const double imagePreScale = 0.5;
const ImageROI roi(0, 0, 512, 336);
const int aboveHorizon = imagePreScale * 210;
const int nthFrame = 5;

ofstream globalParamsFile("params.txt");

/* **************************************************************************************** */
//boost::shared_ptr<Values> updateVelocity(boost::shared_ptr<const rosbag::Bag> bag,
//    boost::shared_ptr<const Values> velocities,
//    const vector<Matrix>& bases, const Sigmas& sigmas, const boost::shared_ptr<ClassPriors>& classPriors)
//{
//  // Set up view to loop over images
//  vector<string> topics;
//  topics.push_back("/camera/image_raw");
//  rosbag::View view(*bag, rosbag::TopicQuery(topics), ros::Time(globalStartTime), ros::Time(globalEndTime));
//
//  // Accumulate a graph on the velocity
//  GaussianFactorGraph graph;
//  SharedDiagonal priorModel = noiseModel::Isotropic::Sigma(bases.front().cols(), 1e6);
//
//  // Loop over messages and accumulate Hessian
//  boost::shared_ptr<ImagePairGrad> imgPair;
//  size_t framenum = 1;
//  BOOST_FOREACH(rosbag::MessageInstance const m, view)
//  {
//    if(sensor_msgs::ImageConstPtr img = m.instantiate<sensor_msgs::Image>())
//    {
//      if(framenum % nthFrame == 0)
//      {
//        // Scale the image using OpenCV
//        cv::Mat imgIn(img->height, img->width, CV_8UC1, const_cast<uint8_t*>(&img->data[0]));
//        cv::Mat imgOut;
//        cv::resize(imgIn, imgOut, cv::Size(), imagePreScale, imagePreScale, CV_INTER_AREA);
//
//        const Eigen::Map<const Eigen::Matrix<uint8_t, -1, -1, Eigen::RowMajor> > imgM(imgOut.data, imgOut.rows, imgOut.cols);
//        if(!imgPair)
//          imgPair = boost::make_shared<ImagePairGrad>(imgM.cast<double>() / 255.0, imgM.cast<double>() / 255.0);
//        else
//          imgPair = boost::make_shared<ImagePairGrad>(*imgPair, imgM.cast<double>() / 255.0);
//        graph += Velocity2Factor(framenum, bases[0], bases[1], sigmas, classPriors, imgPair).linearize(*velocities);
//        graph += PriorFactor<LieVector>(framenum, LieVector(Vector::Zero(bases.front().cols())), priorModel).linearize(*velocities);
//      }
//
//      ++ framenum;
//
//      if(framenum % 100 == 0)
//        cout << "Frame " << framenum << endl;
//    }
//  }
//
//  // Solve for the velocity
//  VectorValues linearSoln = graph.optimize(boost::none, EliminateQR);
//  return boost::make_shared<Values>(velocities->retract(linearSoln));
//}

/* **************************************************************************************** */
Matrix updateRotationBasis(const vector<boost::shared_ptr<const rosbag::Bag> >& bags,
  boost::shared_ptr<Values> velocities, const Matrix& basisRot,
  const Vector& flowSigmas, const Vector& pixSigmas,
  const boost::shared_ptr<vector<vector<Matrix> > >& classPriors, int iteration,
  boost::optional<ros::NodeHandle&> node)
{
  // Store current basis in a Values
  Values::shared_ptr basisValues = boost::make_shared<Values>(basisToValues('r', basisRot));

  // Create graph that will compute the gradient out-of-core
  BasisNGraph basisGraph(bags, nthFrame, imagePreScale, roi, true, true,
    velocities, basisValues, flowSigmas, pixSigmas, classPriors, node, iteration);

  // Optimize
  const GaussianFactorGraph linearized = basisGraph.getAllFactors();
  const VectorValues solnVector = linearized.optimize(boost::none, EliminateQR);
  const Values soln = basisValues->retract(solnVector);

  return basisFromValues('r', soln);
}

/* **************************************************************************************** */
pair<Matrix, vector<Matrix> > updateBases(const vector<boost::shared_ptr<const rosbag::Bag> >& bags,
  bool updateRotationBasis, boost::shared_ptr<Values> velocities,
    const Matrix& basisRot, const vector<Matrix>& basesTrans,
    const Vector& flowSigmas, const Vector& pixSigmas,
    const boost::shared_ptr<vector<vector<Matrix> > >& classPriors, int iteration,
    boost::optional<ros::NodeHandle&> node)
{
  // Store current basis in a Values
  Values::shared_ptr basisValues = boost::make_shared<Values>();
  for(size_t k = 0; k < basesTrans.size(); ++k)
    basisValues->insert(basisToValues('a' + k, basesTrans[k]));
  basisValues->insert(basisToValues('r', basisRot));

  // Create graph that will compute the gradient out-of-core
  BasisNGraph basisGraph(bags, nthFrame, imagePreScale, roi, true, updateRotationBasis,
    velocities, basisValues, flowSigmas, pixSigmas, classPriors, node, iteration);

  GaussianFactorGraph linearized = basisGraph.getAllFactors();
  VectorValues solnVector = linearized.optimize(boost::none, EliminateQR);
  Values soln = basisValues->retract(solnVector);

  Matrix updatedBasisRot;
  vector<Matrix> updatedBasesTrans;
  // Reassemble basis
  for(size_t k = 0; k < basesTrans.size(); ++k)
    updatedBasesTrans.push_back(basisFromValues('a' + k, soln));
  updatedBasisRot = basisFromValues('r', soln);

  return make_pair(updatedBasisRot, updatedBasesTrans);
}

#if 0
/* **************************************************************************************** */
Matrix learnRotationBasis(const vector<boost::shared_ptr<const rosbag::Bag> >& bags,
  const ImageInfo& imageInfo, DenseIndex r,
  double flowSigmaIn, double flowSigmaOut, double pixSigmaIn, double pixSigmaOut,
  double classPriorIn, double classPriorOut,
  int iterations, boost::optional<ros::NodeHandle&> visualizationNode)
{
  // Initialize basis to random
  Matrix basisRot = 2.0 * Matrix::Random(2 * imageInfo.width * imageInfo.height, r)
    - Matrix::Constant(2 * imageInfo.width * imageInfo.height, r, 1.0);

  // Initialize random velocities

  boost::random::mt19937 rng;
  boost::random::uniform_real_distribution<> randdist(-1, 1);
  for(size_t i = 0; i < 1000; ++i) randdist(rng);

  vector<string> topics;
  topics.push_back("/camera/image_raw");
  topics.push_back("camera/image_raw");
  boost::shared_ptr<Values> velocities = boost::make_shared<Values>();
  {
    size_t framenum = 1;
    BOOST_FOREACH(const boost::shared_ptr<const rosbag::Bag>& bag, bags)
    {
      rosbag::View view(*bag, rosbag::TopicQuery(topics));
      for(size_t i = 1; i <= view.size(); ++i)
      {
        if(framenum % nthFrame == 0)
        {
          velocities->insert(framenum, LieVector(Vector::Constant(r, randdist(rng))));
          velocities->insert(symbol('S', framenum), LieMatrix(Matrix::Identity(r, r)));
        }
        ++ framenum;
      }
    }
  }

  // Initialize parameters
  const Vector flowSigmas = (Vector(2) << flowSigmaOut, flowSigmaIn);
  const Vector pixSigmas = (Vector(2) << pixSigmaOut, pixSigmaIn);
  boost::shared_ptr<vector<Matrix> > classPriors = boost::make_shared<vector<Matrix> >(
    2, Matrix(imageInfo.height, imageInfo.width));
  classPriors->at(0).setConstant(classPriorOut);
  classPriors->at(1).setConstant(classPriorIn);

  ros::Publisher basisPublisher =
    visualizationNode->advertise<generalflow_rosmsgs::BasisUpdate>(
    "learnTemplates/learnRotationBasis/basis", 1000);
  ros::Publisher velocityPublisher =
    visualizationNode->advertise<generalflow_rosmsgs::VelocityUpdate>(
    "learnTemplates/learnRotationBasis/velocity", 1000);

  // Optimize
  for(int iteration = 1; iteration <= iterations; ++iteration)
  {
    //cout << "Updating velocity" << endl;
    //velocities = updateVelocity(bag, velocities, bases, sigmas, classPriors);
    cout << "Updating basis" << endl;
    basisRot = updateRotationBasis(bags, velocities, basisRot,
      flowSigmas, pixSigmas, classPriors, iteration, visualizationNode);

    // Send update messages
    basisPublisher.publish(BasisUpdateToRos(imageInfo,
      basisRot, vector<Matrix>(), iteration));
    velocityPublisher.publish(VelocityUpdateToRos(*velocities, 0, iteration));
  }

  return basisRot;
}

/* **************************************************************************************** */
vector<Matrix> learnTranslationBasis(const vector<boost::shared_ptr<const rosbag::Bag> >& bags,
  const ImageInfo& imageInfo, DenseIndex q,
  const Matrix& basisRot,
  double flowSigmaRot, double flowSigmaOut, const Vector& flowSigmasTrans,
  double pixSigmaRot, double pixSigmaOut, const Vector& pixSigmasTrans,
  double classPriorIn, double classPriorOut,
  int iterations, boost::optional<ros::NodeHandle&> visualizationNode)
{
  const DenseIndex nTransBases = flowSigmasTrans.size();
  const DenseIndex r = basisRot.cols();

  // Initialize basis to random
  vector<Matrix> basesTrans;
  basesTrans.reserve(nTransBases);
  for(DenseIndex k = 1; k <= nTransBases; ++k)
  {
    basesTrans.push_back(2.0 * Matrix::Random(2 * imageInfo.width * imageInfo.height, q)
      - Matrix::Constant(2 * imageInfo.width * imageInfo.height, q, 1.0));
  }

  // Initialize random velocities

  boost::random::mt19937 rng;
  boost::random::uniform_real_distribution<> randdist(-1, 1);
  for(size_t i = 0; i < 1000; ++i) randdist(rng);

  vector<string> topics;
  topics.push_back("/camera/image_raw");
  topics.push_back("camera/image_raw");
  boost::shared_ptr<Values> velocities = boost::make_shared<Values>();
  {
    size_t framenum = 1;
    BOOST_FOREACH(const boost::shared_ptr<const rosbag::Bag>& bag, bags)
    {
      rosbag::View view(*bag, rosbag::TopicQuery(topics));
      for(size_t i = 1; i <= view.size(); ++i)
      {
        if(framenum % nthFrame == 0)
        {
          velocities->insert(framenum, LieVector(Vector::Constant(r + q, randdist(rng))));
          velocities->insert(symbol('S', framenum), LieMatrix(Matrix::Identity(r + q, r + q)));
        }
        ++ framenum;
      }
    }
  }

  // Initialize parameters
  const Vector flowSigmas = (Vector(2 + nTransBases) <<
    flowSigmaOut, flowSigmasTrans, flowSigmaRot);
  const Vector pixSigmas = (Vector(2 + nTransBases) <<
    pixSigmaOut, pixSigmasTrans, pixSigmaRot);
  boost::shared_ptr<vector<Matrix> > classPriors = boost::make_shared<vector<Matrix> >(
    2 + nTransBases, Matrix(imageInfo.height, imageInfo.width));
  classPriors->at(0).setConstant(classPriorOut);
  for(DenseIndex k = 1; k <= nTransBases; ++k)
    classPriors->at(k).setConstant(classPriorIn / double(nTransBases + 1));
  classPriors->back().setConstant(classPriorIn / double(nTransBases + 1));

  ros::Publisher basisPublisher =
    visualizationNode->advertise<generalflow_rosmsgs::BasisUpdate>(
    "learnTemplates/learnTranslationBasis/basis", 1000);
  ros::Publisher velocityPublisher =
    visualizationNode->advertise<generalflow_rosmsgs::VelocityUpdate>(
    "learnTemplates/learnTranslationBasis/velocity", 1000);

  // Optimize
  for(int iteration = 1; iteration <= iterations; ++iteration)
  {
    cout << "Updating basis" << endl;
    basesTrans = updateBases(bags, false, velocities, basisRot,
      basesTrans, flowSigmas, pixSigmas, classPriors, iteration, visualizationNode).second;

    // Send update messages
    basisPublisher.publish(BasisUpdateToRos(imageInfo,
      basisRot, basesTrans, iteration));
    velocityPublisher.publish(VelocityUpdateToRos(*velocities, 0, iteration));
  }

  return basesTrans;
}

#endif

/* **************************************************************************************** */
pair<Matrix, vector<Matrix> > learnJointBasis(const vector<boost::shared_ptr<const rosbag::Bag> >& bags,
  const ImageInfo& imageInfo, DenseIndex r, DenseIndex q,
  double flowSigmaRot, double flowSigmaOut, const Vector& flowSigmasTrans,
  double pixSigmaRot, double pixSigmaOut, const Vector& pixSigmasTrans,
  double classPriorIn, double classPriorOut,
  int iterations, boost::optional<ros::NodeHandle&> visualizationNode)
{
  const DenseIndex nTransBases = flowSigmasTrans.size();

  // Initialize basis to random
  Matrix basisRot = 2.0 * Matrix::Random(2 * imageInfo.width * imageInfo.height, r)
    - Matrix::Constant(2 * imageInfo.width * imageInfo.height, r, 1.0);
  vector<Matrix> basesTrans;
  basesTrans.reserve(nTransBases);
  for(DenseIndex k = 1; k <= nTransBases; ++k)
  {
    basesTrans.push_back(2.0 * Matrix::Random(2 * imageInfo.width * imageInfo.height, q)
      - Matrix::Constant(2 * imageInfo.width * imageInfo.height, q, 1.0));
  }

  // Initialize random velocities
  boost::random::mt19937 rng;
  boost::random::uniform_real_distribution<> randdist(-1, 1);
  for(size_t i = 0; i < 1000; ++i) randdist(rng);

  vector<string> topics;
  topics.push_back("/camera/image_raw");
  topics.push_back("camera/image_raw");
  boost::shared_ptr<Values> velocities = boost::make_shared<Values>();
  {
    size_t framenum = 1;
    BOOST_FOREACH(const boost::shared_ptr<const rosbag::Bag>& bag, bags)
    {
      rosbag::View view(*bag, rosbag::TopicQuery(topics));
      for(size_t i = 1; i <= view.size(); ++i)
      {
        if(framenum % nthFrame == 0)
        {
          velocities->insert(framenum, LieVector(Vector::Constant(r + q, randdist(rng))));
          velocities->insert(symbol('S', framenum), LieMatrix(Matrix::Identity(r + q, r + q)));
        }
        ++ framenum;
      }
    }
  }

  // Initialize parameters
  const Vector flowSigmas = (Vector(2 + nTransBases) <<
    flowSigmaOut, flowSigmasTrans, flowSigmaRot);
  const Vector pixSigmas = (Vector(2 + nTransBases) <<
    pixSigmaOut, pixSigmasTrans, pixSigmaRot);

  // Class priors
  boost::shared_ptr<vector<vector<Matrix> > > classPriors = boost::make_shared<vector<vector<Matrix> > >();

  // Rotation data
  classPriors->push_back(vector<Matrix>(2 + nTransBases, Matrix(imageInfo.height, imageInfo.width)));
  classPriors->at(0)[0].setConstant(classPriorOut);
  for(DenseIndex k = 1; k <= nTransBases; ++k)
    classPriors->at(0)[k].setConstant(0.0);
  classPriors->at(0).back().setConstant(classPriorIn);

  // Rotation/translation data
  classPriors->push_back(vector<Matrix>(2 + nTransBases, Matrix(imageInfo.height, imageInfo.width)));
  classPriors->at(1)[0].setConstant(classPriorOut);
  for(DenseIndex k = 1; k <= nTransBases; ++k)
  {
    classPriors->at(1)[k].topRows(aboveHorizon).setConstant(0.0);
    classPriors->at(1)[k].bottomRows(imageInfo.height - aboveHorizon).setConstant(classPriorIn / double(nTransBases + 1));
  }
  classPriors->at(1).back().topRows(aboveHorizon).setConstant(classPriorIn);
  classPriors->at(1).back().bottomRows(imageInfo.height - aboveHorizon).setConstant(classPriorIn / double(nTransBases + 1));

  // Remaining rotation/translation data
  for(size_t bagnum = 2; bagnum < bags.size(); ++bagnum)
    classPriors->push_back(classPriors->at(1)); // Duplicate the first class priors data

  // Start publishers
  ros::Publisher basisPublisher, velocityPublisher;
  if(visualizationNode)
  {
    basisPublisher = visualizationNode->advertise<generalflow_rosmsgs::BasisUpdate>(
        "learnTemplates/learnJointBasis/basis", 100);
    velocityPublisher = visualizationNode->advertise<generalflow_rosmsgs::VelocityUpdate>(
        "learnTemplates/learnJointBasis/velocity", 100);
  }

  // Optimize
  for(int iteration = 1; iteration <= iterations; ++iteration)
  {
    cout << "Updating basis" << endl;
    boost::tie(basisRot, basesTrans) = updateBases(bags, true, velocities, basisRot,
      basesTrans, flowSigmas, pixSigmas, classPriors, iteration, visualizationNode);

    // Send update messages
    if(visualizationNode)
    {
      basisPublisher.publish(BasisUpdateToRos(imageInfo,
          basisRot, basesTrans, iteration));
      velocityPublisher.publish(VelocityUpdateToRos(*velocities, 0, iteration));
    }
  }

  return make_pair(basisRot, basesTrans);
}
/* **************************************************************************************** */
int main(int argc, char* argv[])
{
  //struct rlimit64 limit = { 12000000000, 12000000000 };
  //setrlimit64(RLIMIT_DATA, &limit);
  //setrlimit64(RLIMIT_AS, &limit);

  if(argc < 2)
  {
    cout << "USAGE:  " << argv[0] << " (BAGFILE )+" << endl;
    return 1;
  }

  int argc_ros = 1;
  ros::init(argc_ros, argv, "learnTemplates"); // Pass argc = 1 to not read dataset arguments
  ros::NodeHandle node;

  DenseIndex q = 1;
  DenseIndex r = 3;
  globalParamsFile << "q = " << q << "\n" << "r = " << r << endl;
  globalParamsFile << "imagePreScale = " << imagePreScale << endl;
  globalParamsFile << "nthFrame = " << nthFrame << endl;

  const double flowSigmaOut = 1.5 * imagePreScale;
  const Vector flowSigmasTrans = (Vector(1) << 0.7 * imagePreScale);
  const double flowSigmaRot = 0.7 * imagePreScale;
  const double pixSigmaOut = 0.01;
  const Vector pixSigmasTrans = (Vector(1) << 0.01);
  const double pixSigmaRot = 0.01;

  // Set up class priors
  const double outlierP = 0.005;// 0.15;
  const double inlierP = 0.995; //0.75;

#if 0

  // Open bag files
  vector<boost::shared_ptr<const rosbag::Bag> > bags;
  const string bagfile = argv[1];
  cout << "Opening " << bagfile << "..." << endl;
  bags.push_back(boost::make_shared<rosbag::Bag>(bagfile, rosbag::bagmode::Read));
  ImageInfo imageInfo = getImageInfo(bags.front(), imagePreScale, roi);

  // Learn rotation basis
  Matrix basisRot = learnRotationBasis(bags, imageInfo, r,
                                       flowSigmaRot, flowSigmaOut, pixSigmaRot, pixSigmaOut,
                                       inlierP, outlierP, 20, node);

  // Learn translation basis
  bags.clear();
  for(int i = 1; i < argc - 1; ++i)
  {
    const string bagfile = argv[1 + i];
    cout << "Opening " << bagfile << "..." << endl;
    bags.push_back(boost::make_shared<rosbag::Bag>(bagfile, rosbag::bagmode::Read));
  }

  // Learn translation basis
  vector<Matrix> basesTrans = learnTranslationBasis(bags, imageInfo, q, basisRot,
                                                    flowSigmaRot, flowSigmaOut, flowSigmasTrans, pixSigmaRot, pixSigmaOut, pixSigmasTrans,
                                                    inlierP, outlierP, 50, node);

#else

  // Open bag files
  vector<boost::shared_ptr<const rosbag::Bag> > bags;
  for(int i = 0; i < argc - 1; ++i)
  {
    const string bagfile = argv[1 + i];
    cout << "Opening " << bagfile << "..." << endl;
    bags.push_back(boost::make_shared<rosbag::Bag>(bagfile, rosbag::bagmode::Read));
  }
  ImageInfo imageInfo = getImageInfo(bags.front(), imagePreScale, roi);

  // Learn basis
  Matrix basisRot;
  vector<Matrix> basesTrans;
  boost::tie(basisRot, basesTrans) = learnJointBasis(bags, imageInfo, r, q,
                                                     flowSigmaRot, flowSigmaOut, flowSigmasTrans, pixSigmaRot, pixSigmaOut, pixSigmasTrans,
                                                     inlierP, outlierP, 21, node);
#endif
}
