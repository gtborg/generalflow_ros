/**
 * @file    learningUtils.cpp
 * @brief   
 * @author  Richard Roberts
 * @created Oct 5, 2013
 */

#include "learningUtils.h"

#include <gtsam/base/LieVector.h>
#include <gtsam/base/LieMatrix.h>
#include <gtsam/nonlinear/Values.h>
#include <gtsam/inference/Symbol.h>

#include <fstream>
#include <boost/format.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <rosbag/view.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>

using namespace gtsam;
using namespace std;

namespace generalflow_ros
{

  /* **************************************************************************************** */
  ImageInfo getImageInfo(boost::shared_ptr<const rosbag::Bag> bag, double imagePreScale,
                         const ImageROI& roi)
  {
    vector<string> topics;
    topics.push_back("/camera/image_raw");
    topics.push_back("camera/image_raw");
    rosbag::View view(*bag, rosbag::TopicQuery(topics));

    sensor_msgs::ImageConstPtr img = view.begin()->instantiate<sensor_msgs::Image>();

    if(img->encoding != sensor_msgs::image_encodings::MONO8)
      throw invalid_argument("Currently can only handle 8-bit mono images");

    // Scale the image using OpenCV to be sure we get the correct rounding
    cv::Mat imgIn(img->height, img->width, CV_8UC1, const_cast<uint8_t*>(&img->data[0]));

    cout << "Original image size is " << imgIn.cols << "x" << imgIn.rows << endl;

    if(roi.x >= 0)
      imgIn(cv::Rect(roi.x, roi.y, roi.w, roi.h)).copyTo(imgIn);
    cv::Mat imgOut;
    cv::resize(imgIn, imgOut, cv::Size(), imagePreScale, imagePreScale, CV_INTER_AREA);

    ImageInfo info;
    info.width = imgOut.cols;
    info.height = imgOut.rows;

    cout << "ROIed and scaled image size is " << info.width << "x" << info.height << endl;

    return info;
  }

  /* **************************************************************************************** */
  void writeBasis(int iteration, const Matrix& basis)
  {
    ofstream out((boost::format("generalflow_ros_learn_%06d_basis.txt") % iteration).str().c_str());
    out << basis;
  }

  /* **************************************************************************************** */
  void writeVelocities(int iteration, boost::shared_ptr<const Values> values)
  {
    ofstream out((boost::format("velocity_%04d.txt") % iteration).str().c_str());
    BOOST_FOREACH(const Values::Filtered<LieVector>::ConstKeyValuePair& v, values->filter<LieVector>()) {
      out << v.value.transpose() << "\n";
    }
  }

  /* **************************************************************************************** */
  gtsam::Values basisToValues(unsigned char keyChar, const gtsam::Matrix& basis)
  {
    Values basisValues;
    for(size_t pix = 0; pix < basis.rows() / 2; ++pix)
      basisValues.insert(symbol(keyChar, pix), LieMatrix(basis.middleRows(2*pix, 2)));
    return basisValues;
  }

  /* **************************************************************************************** */
  gtsam::Matrix basisFromValues(unsigned char keyChar, const gtsam::Values& values)
  {
    // Look for size
    DenseIndex nPix = 0;
    for(Values::const_iterator it = values.find(symbol(keyChar, 0));
      it != values.end() && symbolChr(it->key) == keyChar; ++it)
      ++ nPix;

    if(nPix > 0)
    {
      DenseIndex cols = values.at<LieMatrix>(symbol(keyChar, 0)).cols();

      // Reassemble basis
      Matrix basis(2*nPix, cols);
      for(size_t pix = 0; pix < nPix; ++pix)
        basis.middleRows(2*pix, 2) = values.at<LieMatrix>(symbol(keyChar, pix));

      return basis;
    }
    else
    {
      return Matrix();
    }
  }

}
