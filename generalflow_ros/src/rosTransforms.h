/**
 * @file    rosTransforms.h
 * @brief   Utilities for converting to and from ROS messages
 * @author  Richard Roberts
 * @created Dec 17, 2013
 */

#pragma once

#include "dllexport.h"
#include <generalflow_rosmsgs/FullImageErrorVisualization.h>
#include <generalflow_rosmsgs/BasisUpdate.h>
#include <generalflow_rosmsgs/VelocityUpdate.h>
#include <generalflow_flow/flowOps.h>
#include "learningUtils.h"

#include <gtsam/nonlinear/Values.h>

namespace generalflow_ros
{
  generalflow_ros_EXPORT
    generalflow_rosmsgs::FullImageErrorVisualizationConstPtr
    FullImageErrorVisualizationToRos(const generalflow::flow::FullImageErrorVisualization& visualization, const ros::Time& stamp);

  generalflow_ros_EXPORT
    generalflow_rosmsgs::BasisUpdateConstPtr
    BasisUpdateToRos(const ImageInfo& imageInfo,
    const gtsam::Matrix& basisRot, const std::vector<gtsam::Matrix>& basesTrans, int iteration);

  generalflow_ros_EXPORT
    generalflow_rosmsgs::VelocityUpdateConstPtr
    VelocityUpdateToRos(const gtsam::Values& velocityValues, unsigned char keyChar, int iteration);
}
